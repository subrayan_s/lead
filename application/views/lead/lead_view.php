<div class="container-fluid">
   <div class="panel panel-default">
      <div class="panel-heading" style="padding:5px 5px;" >
         <div class="col-sm-6">
            <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;top:8px;"></span>  <span style="font-size:18px;">
            <?php if($details->name != ''){echo $details->name ;} else { echo 'Jimmy Anderson'; } ?>
            </span>
         </div>
         <div class="col-sm-6">
            <div class=" row pull-right">
               <button type="button"  title="Details" id="btn1" class="btn btn-default btn-md">
               <span class="glyphicon glyphicon-th"></span> Details
               </button>
               <button  title="Activity" type="button" id="btn2" class="btn btn-default btn-md">
               <span class="glyphicon glyphicon-th-list"></span> Related
               </button>
            </div>
         </div>
         <div class="panel-body" style="padding:4px;"></div>
      </div>
   </div>
   <div class="panel-group relateddata">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>Lead Information</h4>
         </div>
         <div class="panel-body">
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>First Name :</strong></label>
                  <span class="form-control-static"><?php echo $details->firstname; ?></span>
               </div>
               <div class="form-group">
                  <label><strong>Last Name:</strong></label>
                  <span class="form-control-static"><?php echo $details->lastname; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Email :</strong></label>
                  <span class="form-control-static"><?php echo $details->email; ?></span>
               </div>
               <div class="form-group">
                  <label><strong>Mobile </strong></label>
                  <span class="form-control-static"><?php echo $details->mobilephone; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Phone :</strong></label>
                  <span  class="form-control-static"><?php echo $details->phone; ?></span>
               </div>
               <div class="form-group">
                  <label><strong>Company</strong></label>
                  <span class="form-control-static"><?php echo $details->company; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
            </div>
         </div>
      </div>
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>Lead Details</h4>
         </div>
         <div class="panel-body">
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Request Type :</strong></label>
                  <span  class="form-control-static"><?php echo $details->request_type__c ?></span>
               </div>
               <div class="form-group">
                  <label><strong>Lead type :</strong></label>
                  <span  class="form-control-static"><?php echo $details->lead_type__c; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Selling Dealer :</strong></label>
                  <span  class="form-control-static"><?php echo $details->selling_dealer__c; ?></span>
               </div>
               <div class="form-group">
                  <label><strong>Vehicle Type :</strong></label>
                  <span  class="form-control-static"><?php echo $details->vehicle_type__c; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Stock No :</strong></label>
                  <span  class="form-control-static"><?php echo $details->stock_number__c; ?></span>
               </div>
               <div class="form-group">
                  <label><strong>Model :</strong></label>
                  <span  class="form-control-static"><?php echo $details->model__c; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Test Drive :</strong></label>
                  <?php if( $details->eval_lead__c=='t'){ ?>
                  <input type="checkbox"  name="edit_financemanager" id="edit_financemanager" value="1" checked disabled>
                  <?php
                     }else{ ?>
                  <input type="checkbox"  name="edit_financemanager" id="edit_financemanager" value="1" disabled>
                  <?php
                     }?>
               </div>
               <div class="form-group">
                  <label><strong>Lead Source</strong></label>
                  <span class="form-control-static"><?php echo $details->leadsource; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Lead Status</strong></label>
                  <span  class="form-control-static"><?php echo $details->status; ?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Introduction to Finance Manager :</strong></label>
                  <?php if( $details->introduction_to_finance_manager__c =='t'){ ?>
                  <input type="checkbox"  name="edit_financemanager" id="edit_financemanager" value="1" checked disabled>
                  <?php
                     }else{ ?>
                  <input type="checkbox"  name="edit_financemanager" id="edit_financemanager" value="1" disabled>
                  <?php
                     }?>
               </div>
            </div>
         </div>
      </div>
	  
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>Address Details</h4>
         </div>
         <div class="panel-body">
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Street</strong></label>
                  <span  class="form-control-static"><?=$details->street?></span>
               </div>
               <div class="form-group">
                  <label><strong>City</strong></label>
                  <span  class="form-control-static"><?=$details->city?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>State/Province</strong></label>
                  <span  class="form-control-static"><?=$details->state?></span>
               </div>
               <div class="form-group">
                  <label><strong>Zip/PostalCode</strong></label>
                  <span  class="form-control-static"><?=$details->postalcode?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Country</strong></label>
                  <span  class="form-control-static"><?=$details->country?></span>
               </div>
            </div>
         </div>
      </div>	  
	  
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>Customer Insight</h4>
         </div>
         <div class="panel-body">
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Previous Customer Name</strong></label>
                  <span  class="form-control-static"><?=$details->previous_customer_name__c?></span>
               </div>
               <div class="form-group">
                  <label><strong>Previous Salesman Name</strong></label>
                  <span  class="form-control-static"><?=$details->previous_salesman_name__c?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Previous Model Of Interest</strong></label>
                  <span  class="form-control-static"><?=$details->previous_model_of_interest__c?></span>
               </div>
               <div class="form-group">
                  <label><strong>Previous Lead Status</strong></label>
                  <span  class="form-control-static"><?=$details->previous_lead_status__c?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Service Life Time Value</strong></label>
                  <span  class="form-control-static"><?=$details->service_lifetime_value__c?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Sales Life Time Value</strong></label>
                  <span  class="form-control-static"><?=$details->sales_lifetime_value__c?></span>
               </div>
            </div>			
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Activity Score</strong></label>
                  <span  class="form-control-static"><?=$details->activity_score__c?></span>
               </div>
            </div>	
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong> Customer Data Completeness Score</strong></label>
                  <span  class="form-control-static"><?=$details->customer_data_completeness_score__c?></span>
               </div>
            </div>			
         </div>
      </div>	

      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>Addtional Information</h4>
         </div>
         <div class="panel-body">
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Product Interest</strong></label>
                  <span  class="form-control-static"><?=$details->productinterest__c?></span>
               </div>
               <div class="form-group">
                  <label><strong>Current Generator(s)</strong></label>
                  <span  class="form-control-static"><?=$details->currentgenerators__c?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>SIC code</strong></label>
                  <span  class="form-control-static"><?=$details->siccode__c?></span>
               </div>
               <div class="form-group">
                  <label><strong>Primary</strong></label>
                  <span  class="form-control-static"><?=$details->primary__c?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Number of locatios</strong></label>
                  <span  class="form-control-static"><?=$details->numberoflocations__c?></span>
               </div>
            </div>		
         </div>
      </div>	
	  
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>System Information</h4>
         </div>
         <div class="panel-body">
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Created by</strong></label>
                  <span  class="form-control-static"><?=$details->createdbyid?></span>
               </div>
               <div class="form-group">
                  <label><strong>Created Date</strong></label>
                  <span  class="form-control-static"><?=$details->createddate?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Last modified by</strong></label>
                  <span  class="form-control-static"><?=$details->lastmodifiedbyid?></span>
               </div>
               <div class="form-group">
                  <label><strong>Last modified date</strong></label>
                  <span  class="form-control-static"><?=$details->lastmodifieddate?></span>
               </div>
            </div>	
         </div>
      </div>		  
	  
   </div>
   <div class="container-fluid activitydata">
      <div class="row">
         <div class="col-sm-8">
            <div class="panel-group">
               <div class="panel panel-default">
                  <div class="panel-heading clearfix" style="background-color:#fff; border-bottom:1px  solid #ddd;">
                     <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Open Activities</h4>
                     <div class="btn-group pull-right">
                        <a href="#" class="btn btn-default btn-sm">New Task</a>
                     </div>
                  </div>
               </div>
               <div class="panel panel-default" style="margin-top: 10px;">
                  <div class="panel-heading clearfix" style="background-color:#fff; border-bottom:1px  solid #ddd;">
                     <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Campaign History(1)</h4>
                     <div class="btn-group pull-right">
                        <a href="#" class="btn btn-default btn-sm">Add to Campaign</a>
                     </div>
                  </div>
                  <div class="panel-body">
                     <table class="table table-condensed">
                        <thead>
                           <tr>
                              <th>CAMPAIGN NAME</th>
                              <th>START DATE</th>
                              <th>TYPE</th>
                              <th>STATUS</th>
                              <th> </th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>
                                 <a href="">
                                    No Make No Model
                              </td>
                              <td></td>
                              <td>Event</td>
                              <td>Sent</td>
                              <td></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="panel panel-default" style="margin-top: 10px;">
               <div class="panel-heading clearfix" style="background-color:#fff; border-bottom:1px  solid #ddd;">
               <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Note</h4>
               <div class="btn-group pull-right">
               <a href="#" class="btn btn-default btn-sm">New Note</a>
               </div>
               </div>
               <div class="panel-body">
               </div>
               </div>
               <div class="panel panel-default" style="margin-top: 10px;">
                  <div class="panel-heading clearfix" style="background-color:#fff; border-bottom:1px  solid #ddd;">
                     <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Attachment</h4>
                     <div class="btn-group pull-right">
                        <a href="#" class="btn btn-default btn-sm">Attachment</a>
                     </div>
                  </div>
                  <div class="panel-body">
                  </div>
               </div>
               <div class="panel panel-default" style="margin-top: 10px;">
                  <div class="panel-heading" style="background-color:#fff;border-bottom:1px  solid #ddd;">
                     <h4>Activity History</h4>
                  </div>
                  <div class="panel-body">
                  </div>
               </div>
               <div class="panel panel-default" style="margin-top: 10px;">
                  <div class="panel-heading" style="background-color:#fff;border-bottom:1px  solid #ddd;">
                     <h4>Lead History (1)</h4>
                  </div>
                  <div class="panel-body">
                     <table class="table table-condensed">
                        <thead>
                           <tr>
                              <th>Date</th>
                              <th>Field</th>
                              <th>User</th>
                              <th>Original value</th>
                              <th>New value</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>7/8/2018 3:38 AM</td>
                              <td>Created.</td>
                              <td>Nicholas Markham</td>
                              <td></td>
                              <td></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>