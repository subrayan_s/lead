<style>
.red_star{
	font-size:13px;
	color:red;
	font-weight:400;
}	
</style>
<div class="container-fluid" >
   <div id="wait" class="loader"></div>
   <div id="displayResult"> </div>
   <div class="row" style="margin-bottom:20px;">
      <div class="col-sm-6">
         <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;"></span>  <span style="font-size:18px;">Leads</span>
      </div>
      <div class="col-sm-6">
         <div class="pull-right">
            <button id="createNewLead" type="button" class="btn btn-default " data-toggle="modal" data-target="#createLeadModal">New</button>&nbsp;
            <button onclick="loadDataTable('lead/initDataTable')" type="button" class="btn btn-default ">Refresh</button>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-12">
         <table class="table table-hover" id="example">
            <thead>
               <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Company</th>
                  <th>Email</th>
                  <th>MobilePhone</th>
				  <th> LeadType</th>
				  <th> RequestType </th>
				  <th> LeadSource </th>
				  <th> Status</th>			
				  <th> VehicleType</th>		
				  <th> CreatedDate</th>			
				  
                  <th>Edit </th>
               </tr>
            </thead>
            <tbody>
               <?php $no = 1;
                  foreach($lead_details as $new_lead_details){
					  $id = $new_lead_details->id;
					  $firstname = $new_lead_details->firstname;
					  $lastname = $new_lead_details->lastname;
					  $name = $firstname.' '.$lastname;
					  $email = $new_lead_details->email;
					  $mobilephone = $new_lead_details->mobilephone;
					  $street = $new_lead_details->street;
					  $city   = $new_lead_details->city;
					  $state  = $new_lead_details->street;
					  $zipc   = $new_lead_details->city;	
					  $country= $new_lead_details->country;
					  $address = $street.' '.$city.' '.$state.' '.$zipc.' '.$country;
					  $company = $new_lead_details->company; 
					  $recordType = $new_lead_details->lead_type__c;
					  $request_type__c = $new_lead_details->request_type__c;
					  $owner = $new_lead_details->ownerid;
                  ?>
               <tr>
                  <td><?php echo $no;  ?></td>
                  <td><a href="<?=WEB_DIR.'lead/lead_view/'.$id;?>"><?php echo $name;  ?></a></td>
                  <td><?php echo $company;  ?></td>
                  <td><?php echo $email;  ?></td>
                  <td><?php echo $mobilephone;  ?></td>
				  <td><?php echo $recordType; ?></td>
				  <td><?php echo $request_type__c ; ?></td>
				  <td><?=$new_lead_details->leadsource?></td>
				  <td><?=$new_lead_details->status?></td>
				  <td><?=$new_lead_details->vehicle_type__c?></td>
				  <td><?=$new_lead_details->createddate?></td>
                  <td>
                     <a data-toggle="modal" title="View Lead" data-id="<?php echo $id; ?>" class="open-leadEdit" data-target="#createLeadModal"><span style="font-size:15px;" class="glyphicon glyphicon-edit"></span></a> 
                  </td>
               </tr>
               <?php $no++; } ?>
            </tbody>
         </table>
      </div>
   </div>
</div>

<!-- Insert and Edit Modal ------------------------------------> 
<div id="createLeadModal" class="modal fade" role="dialog" >
   <div class="modal-dialog">
	<form method="post" id="insertAndEditForm" enctype="multipart/form-data"> 
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close clearData" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center" id="modalDecide"></h4>
         </div>
		<div class="modal-body" id="accordion_new" style="padding: 5px;">
		   <div style="margin-left:20px; margin-right:15px;">
			  <div class="panel panel-default">
				 <div class="panel-heading">&nbsp;Lead Information</div>
				 <div class="panel-body">
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label><span class="red_star">*</span>Name</label></div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Lead Owner</label>
							  <div type="text" id="ownerid" > <b class="m-2">&nbsp;<?=$this->session->userdata("dealer")?></b> </div>
						  </div>
					   </div>
					</div>
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Salutation</label>
							<select name="salutation" id="salutation" class="form-control">
								<option value="mr">Mr.</option>
								<option value="miss">Miss.</option>
								<option value="mrs">Mrs.</option>
							 </select>
						  </div>
					   </div>
					</div>	
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>First Name</label>
								<input name="firstname" type="text" class="form-control" id="firstname">
						  </div>
					   </div>
					</div>	
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label><span class="red_star">*</span>Last Name</label>
							 <input type="text" name="lastname"  id="lastname" class="form-control" required>
						  </div>
					   </div>
					</div>							
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label><span class="red_star">*</span>Company</label>
							 <input type="text" name="company" id="company" class="form-control" required>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Lead Record Type</label>
							 <input type="text" class="form-control" name="record_type_name__c" id="record_type_name__c" /> 
						  </div>
					</div>	
					</div>
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>No. Of Employees</label>
							 <input type="number" id="numberofemployees" name="numberofemployees" class="form-control" value=1>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Selling Dealer</label>
								<input  name="selling_dealer__c" id="selling_dealer__c" class="form-control" type="search" />
								<span onclick="showSearchModal('selling_dealer__c')" class="glyphicon glyphicon-search"></span>
								<div style="display:none;" id="selling_dealer__c_error"> </div>
		 				  </div>
					   </div>
					</div>
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Source</label>
							 <input type="text" class="form-control" id="source__c" name="source__c">
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Phone</label>
							 <input type="text" name="phone" id="phone" class="form-control">
						  </div>
					   </div>
					</div>
					
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label><span class="red_star">*</span>Lead Source</label>
							 <select id="leadsource" name="leadsource" class="form-control" required>
								<option value="">-- SELECT --</option>
								<option value="Phone"> Phone </option>
								<option value="Walk-In"> Walk-In </option>
								<option value="Internet"> Internet</option>
							 </select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Mobile</label>
							 <input type="text" class="form-control" name="mobilephone" id="mobilephone">
						  </div>
					   </div>
					</div>
					
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Lead Type</label>
							 <select class="form-control" id="lead_type__c" name="lead_type__c">
							 </select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Fax</label>
							 <input type="text" class="form-control" id="fax" name="fax">
						  </div>
					   </div>
					</div>	
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label><span class="red_star">*</span>Lead Status</label>
							 <select id="status" name="status" class="form-control" required>
								<option value="">-- SELECT --</option>
								<option value="working"> Working</option>
								<option value="notWorking"> Not Working</option></select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Email</label>
							 <input type="text" id="email" name="email" class="form-control">
						  </div>
					   </div>
					</div>	
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label ><span class="red_star">*</span>Request Type</label>
							 <select id="request_type__c" name="request_type__c" class="form-control" required>
								<option value="">-- SELECT --</option>
								<option value="TestDrive"> Test Drive</option>
							</select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Website</label>
							 <input id="website" name="website" type="text" class="form-control" />
						  </div>
					   </div>
					</div>	

					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label ><span class="red_star">*</span>Vehicle Type</label>
							 <select name="vehicle_type__c" id="vehicle_type__c" class="form-control" required>
								<option value="">-- SELECT --</option>
								<option value="used"> used </option>
								<option value="new"> new</option>
							 </select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Description</label>
							 <textarea id="description" name="description" class="form-control"></textarea>
						  </div>
					   </div>
					</div>	
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Interest Of Make</label>
							 <input type="text" class="form-control" id="interest_of_make__c" name="interest_of_make__c" />
							 <span onclick="showSearchModal('interest_of_make__c')" class="glyphicon glyphicon-search"></span>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Stock Number</label>
							 <input type="text" class="form-control" id="stock_number__c" name="stock_number__c" />
						  </div>
					   </div>
					</div>

					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Interest Of Model</label>
							 <select id="interest_of_model__c" name="interest_of_model__c" class="form-control">
								<option value="">-- SELECT --</option>
								<option value="Phone"> Phone </option>
								<option value="Walk-In"> Walk-In </option>
							 </select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Stock Image</label>
							 <input id="stock_image__c" name="stock_image__c" type="text" class="form-control" />
						  </div>
					   </div>
					</div>	
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Industry</label>
							 <select id="industry" name="industry" class="form-control">
								<option value="">-- SELECT --</option>
								<option value="Phone"> Phone </option>
								<option value="Walk-In"> Walk-In </option>
							 </select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Finance Status</label>
							 <select id="finance_status__c" name="finance_status__c" class="form-control">
								<option value="">-- SELECT --</option>
								<option value="Phone"> Phone </option>
								<option value="Walk-In"> Walk-In </option>
							 </select>
						  </div>
					   </div>
					</div>	
					
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Indrotuction To Finance Manager</label>
							 <br />
							 <input type="checkbox" name="introduction_to_finance_manager__c"id="introduction_to_finance_manager__c" />
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Send To DMS</label><br />
							<input id="sent_to_dealer__c" name="sent_to_dealer__c" type="checkbox" />
						  </div>
					   </div>
					</div>		

					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Rating</label>
							 <select id="rating" name="rating" class="form-control">
								<option value="1"> 1</option>
								<option value="2"> 2 </option>
							 </select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Test Drive</label><br />
							<input type="checkbox" id="eval_lead__c" name="eval_lead__c" />
						  </div>
					   </div>
					</div>		

					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Stock Number</label>
							 <input type="text" name="vehicle_stock__c" id="vehicle_stock__c" class="form-control" />
								
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Valuation</label><br />
								<input name="appraisal_flag_field__c" id="appraisal_flag_field__c" type="checkbox" />
						  </div>
					   </div>
					</div>		
					
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Comments From Stock</label>
							 <textarea name="comments_from_stock_c__c" id="comments_from_stock_c__c" class="form-control" ></textarea>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Model Name</label>
							 <input class="form-control" type="text" id="model_name_lp__c" name="model_name_lp__c"/>
						  </div>
					   </div>
					</div>		
					
				 </div>
			  </div>
			  <div class="panel panel-default">
				 <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion_new" href="#Address_Information"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;Address Information</div>
				 <div id="Address_Information" class="panel-body collapse in" aria-expanded="true">
				  <div class="row">
					  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">Address</div>
				  </div>					
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Street</label>
								<textarea id="street" name="street" class="form-control"cols="5" row="20"> </textarea>
						  </div>
					   </div>
					</div>
					 <div class="row">
					   <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>City</label>
								<input id="city" name="city" type="text" class="form-control" />
						  </div>
					   </div>
					   <div class="col-lg-2 col-md-2 col-sm-2" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>State/Province</label>
								<input id="state" name="state" type="text" class="form-control" />
						  </div>
					   </div>						   
					</div>
					 <div class="row">
					   <div class="col-lg-4 col-md-4 col-sm-4" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Zip/PostalCode</label>
								<input id="postalcode" name="postalcode" type="text" class="form-control" />
						  </div>
					   </div>
					   <div class="col-lg-2 col-md-2 col-sm-2" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Country</label>
								<input id="country" name="country" type="text" class="form-control" />
						  </div>
					   </div>						   
					</div>						
					<!--- end of row -->						
				 </div>
			  </div>
			  <!-- Panel End -->
			   <div class="panel panel-default">
				 <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion_new" href="#customer_insight"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;Customer Insight</div>
				 <div id="customer_insight" class="panel-body collapse in" aria-expanded="true">
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Previous Customer Name</label>
							 <input type="text" id="previous_customer_name__c" name="previous_customer_name__c" class="form-control" name="leadSource" />
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Previous salesman name</label>
							 <input id="previous_salesman_name__c" name="previous_salesman_name__c" type="text" class="form-control" />
						  </div>
						</div>	
					</div>
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Previous Model Of Interest</label>
							 <input id="previous_model_of_interest__c"  name="previous_model_of_interest__c"type="text" class="form-control" />
						  </div>
						</div>							
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Previous Lead Status</label>
							 <select id="previous_lead_status__c" name="previous_lead_status__c" class="form-control">
								<option value="">-- SELECT --</option>
								<option value="Phone"> Phone </option>
								<option value="Walk-In"> Walk-In </option>
							 </select>
						  </div>
					   </div>
					</div>	
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Service Life Time Value</label>
							 <input id="service_lifetime_value__c" name="service_lifetime_value__c" type="number" value=0 class="form-control">
						  </div>
						</div>							
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Sales Life Time Value</label>
							 <input type="number" id="sales_lifetime_value__c" name="sales_lifetime_value__c" class="form-control" value=0 />
								
						  </div>
					   </div>
					</div>								
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Activity Score</label>
							 <input id="activity_score__c" name="activity_score__c" type="number" class="form-control" value=0 />
						  </div>
						</div>	
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Customer Data Completeness Score</label>
							 <input id="customer_data_completeness_score__c" name="customer_data_completeness_score__c" type="number" class="form-control" value=0 />
						  </div>
						</div>									
					</div>						
					<!--- end of row -->						
				 </div>
			  </div>

				<div class="panel panel-default">
				 <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion_new" href="#addtionalInfo"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;Addtional Information</div>
				 <div id="addtionalInfo" class="panel-body collapse in" aria-expanded="true">
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Product Interest</label>
							 <select id="productinterest__c" name="productinterest__c" class="form-control">
								<option value="">-- SELECT --</option>
								<option value="Phone"> Phone </option>
								<option value="Walk-In"> Walk-In </option>
							 </select>
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Current Generator(s)</label>
							 <input id="currentgenerators__c" name="currentgenerators__c"  type="text" class="form-control" />
						  </div>
						</div>	
					</div>
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>SIC code</label>
							 <input id="siccode__c" name="siccode__c" type="text" class="form-control">
						  </div>
						</div>							
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Primary</label>
							 <select id="primary__c" name="primary__c" class="form-control">
								<option value="">-- SELECT --</option>
								<option value="Phone"> Phone </option>
								<option value="Walk-In"> Walk-In </option>
							 </select>
						  </div>
					   </div>
					</div>						
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label >Number of locatios</label>
							 <input id="numberoflocations__c"  name="numberoflocations__c"  type="number" class="form-control" value=0 />
						  </div>
						</div>							
					</div>						
					<!--- end of row -->						
				 </div>
			  </div>
				<div class="panel panel-default">
				 <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion_new" href="#system_info"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;System Information</div>
				 <div id="system_info" class="collapse in panel-body" aria-expanded="true">
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Created by</label>
								<div id="createdby" ></div>	
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Last modified by</label>
							 <div id="lastmodifiedby"></div>	
						  </div>
						</div>	
					</div>
					<div class="row">
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Created Date</label>
								<div id="createddate" ></div>		
						  </div>
					   </div>
					   <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
						  <div class="form-group">
							 <label>Last modified date</label>
							 <div id="lastmodifiedbyid"></div>	
						  </div>
						</div>	
					</div>						
					<!--- end of row -->						
				 </div>
			  </div>				  
			   <!-- Panel End -->
		   </div>
		</div>
		<div class="modal-footer">
		   <button type="button" class="clearData btn btn-default" data-dismiss="modal">Cancel</button>
		   <input type="hidden" id="addOrEdit" />
		   <input type="hidden" id="editIDadd" name="editID" />
		   <button class="lead_update btn btn-default btn-info" value="Save">Save</button>
		</div>
	 </form>
  </div>
</div>
</div>
<!-- Insert and Edit Modal Ends -->


<!-- Search Modal -->
<div id="SearchModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Search</h4>
      </div>
      <div class="modal-body">
        <input id="sdname" class="form-control" type="search" />
		<span style="padding:21px 27px;" class="searchSD glyphicon glyphicon-search"></span>
		<hr />
		<input type="hidden" id="getSearchField" />
		<div class="text-center Search_results_div"> </div><br />
		<table class="table table-bordered">
			<tbody id="modalSearchRes">
				
			</tbody>
		</table>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo WEB_DIR;?>assets/js/lead.js"></script>
