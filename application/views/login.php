

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">       
        <meta name="viewport" content="width=device-width, initial-scale=1" user-scalable="no">
        <meta name="description" content="Audi Australia">
        <title>Lead Creation</title>


        


        <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/bootstrap.css" /> 
        
        <link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/logincustom.css" /> 
           
 
    </head>
    <body style="background-image:url('<?php echo WEB_DIR;?>assets/images/auth-bg.jpg')">
        <div id="page-top" class="wrapper">

        
			
			 <div   id="page-wrapper">
                             <div class="container-fluid" id="page-section">
                             <div class="row" id="login-win">
                                 <div class="container" >
								 
								
								
       				 
								 
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-10 col-lg-offset-4 col-md-offset-4 col-sm-offset-4  col-xs-offset-1 audi-box-shadow audi-v-center zoomIn animated faster " style="padding: 20px !important; background-color:#fff;">
                                    	<?php  if($this->session->userdata('login_error') == 1)
				{ 	
			
			    $this->session->unset_userdata('login_error');  
			    
			?>
				
					<h4 style="margin-top:10px;" class="text-danger text-center"> Invalid  Username or Password</h4>
				<?php 
				} ?>
			                  

                            <div id="mod-cont" style="padding: 0 10px 0 20px;" >
                                <div class="row">
                                    <div class="text-center">
                                     <center> <img src="<?php echo WEB_DIR;?>assets/images/logo-dark.png" class="img-responsive" style="height:70px">
                                    </center>  </div>
                                </div>
                               
                                <div class="row">                                    
                                    <h4 class=" text-center">Log into your account</h4> 
                                    <hr class="audi-dotted-style"/>
                                </div>                                 
                                
                                <form id="login_form" action="<?php echo WEB_DIR;?>login/verify/" data-parsley-validate  method="post">

                                 
									
									  <div class="form-group">


            <input type="text" name="email" class="form-control" id="inputEmail" placeholder="User Name" data-parsley-required-message="Username is required" required>

        </div>

        <div class="form-group">

            <input type="password" name="password" class="form-control" id="inputPassword" data-parsley-required-message="Password is required" placeholder="Password" required>

        </div>
		
		
		 <button type="submit" class="btn btn-md btn-danger pull-right">
  Login
  </button>
  
 
  
                                </form>            
                            </div>
                       
                    
               
            

            <!-- Modal  End-->
            

                </div>
                                 </div>
                             </div>  
                             
                             </div> 

            </diV>


          
          
        </div>




   
    </div>

            <script type="text/javascript" src="<?php echo WEB_DIR;?>assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo WEB_DIR;?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo WEB_DIR;?>assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="<?php echo WEB_DIR;?>assets/js/parsley.js"></script>
		
	<script>
	$(document).ready(function() { 
 $('#login_form').submit(function() {
    $("#wait").show();
});
/* code here */ });

 $("#errorMessage").delay(4500).fadeOut(150);

    </script>
      

    </body>
</html>

 
