<footer class="page-footer font-small blue pt-4" style="border-top:1px solid #1E2443; ">

  

    
    <div class="footer-copyright text-center py-3">© 2018 All Rights Reserved.
      <a href="https://www.qrsolutions.com.au/"> QRSolutions</a>
    </div>
   

  </footer>
 
 
 	<script>	
	$(document).on("click", ".open-leadEdit", function () 
	{
	

  $('#editform').find('input:text').val('');  
  $('#editform').find('input:selected').val('');  
  
     var stockid = $(this).data('id');
	 
     $(".modal-body #stock_id").val(stockid);
	 url = "<?php echo WEB_DIR; ?>/stock/stockeditFetch";

	   $("#wait").show();
	 
		$.ajax({
						url: url,
						type: 'POST',
						data: { stock_id : stockid },
						dataType: 'json',
						cache: false,
						success: function (data) 
						{
						name = data.name;
						sellingdealer = data.sellingdealer;
						vehicletype = data.vehicle_type;
						fuel = data.fuel;
						model = data.model;
						vin = data.vin;
						regno = data.regno;
						year = data.year;
						bodytype = data.bodytype;
						bodycolor = data.bodycolor;
						price = data.price;
						series = data.series;
					

									
						 $("#edit_stockno").val(name);
						 $("#edit_sellingdealer").val(sellingdealer);
						 $("#edit_vehicletype").val(vehicletype);
						 $("#edit_fueltype").val(fuel);
						 $('#edit_model').val(model);
						 $('#edit_vin').val(vin);
						 $('#edit_regno').val(regno);
						 $('#edit_year').val(year);
						 $('#edit_bodytype').val(bodytype);
						 $('#edit_bodycolor').val(bodycolor);
						 $('#edit_price').val(price);
						 $('#edit_series').val(series);
						 
						
					
						  $("#wait").hide();
						//$('.modal-body').html(data);							
						}
					});
    });
	
	
	
	/*
	 $('.formCheck').parsley().on('form:validate', function (formInstance) 
	 {
	 final_check = $('.formCheck').parsley().isValid();
					 
		              if(final_check==true)
						 {

						 }
						else
						{
						
						}
	 });
	 
*/

	</script>
 
 <script>
$( document ).ready(function() {
   $(".activitydata").hide();

$( "#btn1" ).removeClass( "btn-default" );

$( "#btn1" ).addClass( "btn-primary" )
});

$( "#btn1" ).click(function() {
  $(".activitydata").hide();
   $(".relateddata").show();

$( "#btn2" ).removeClass( "btn-primary" );

$( "#btn2" ).addClass( "btn-default" )

$( "#btn1" ).removeClass( "btn-default" );

$( "#btn1" ).addClass( "btn-primary" )

});
$( "#btn2" ).click(function() {
   $(".activitydata").show();
   $(".relateddata").hide();
$( "#btn1" ).removeClass( "btn-primary" );

$( "#btn1" ).addClass( "btn-default" )

$( "#btn2" ).removeClass( "btn-default" );

$( "#btn2" ).addClass( "btn-primary" )
});
</script>
 
</body>
</html>