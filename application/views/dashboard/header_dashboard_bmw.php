<!DOCTYPE html>


<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/dataTables.bootstrap.css" />
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/bootstrap.min.css" />
  
  	<link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/css/jquery.jqChart.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_DIR;?>assets/styles.css" />
	
	<script src="<?php echo WEB_DIR;?>assets/js/jquery.jqChart.min.js" type="text/javascript"></script>
   	<script src="<?php echo WEB_DIR;?>assets/js/jquery.jqGauges.min.js" type="text/javascript"></script>
  
  <link rel="stylesheet" href="https://portal.erpapps.com/leadapp/css/animations.css">
 
 
	
  <script src="<?php echo WEB_DIR;?>assets/js/bootstrap.min.js"></script>
  

<script src="<?php echo WEB_DIR;?>assets/js/jquery.dataTables.min.js"></script> 
<script src="<?php echo WEB_DIR;?>assets/js/dataTables.bootstrap.min.js"></script> 


  <script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var gradient1 = {
                type: 'linearGradient',
                x0: 0,
                y0: 0.5,
                x1: 0,
                y1: 0.5,
                colorStops: [{ offset: 0, color: '#C5F80B' },
                             { offset: 1, color: '#6B8901'}]
            };

            var gradient2 = {
                type: 'linearGradient',
                x0: 0.5,
                y0: 0,
                x1: 0.5,
                y1: 0,
                colorStops: [{ offset: 0, color: '#FF3366' },
                             { offset: 1, color: '#B2183E'}]
            };

            var anchorGradient = {
                type: 'radialGradient',
                x0: 0.35,
                y0: 0.35,
                r0: 0.0,
                x1: 0.35,
                y1: 0.35,
                r1: 1,
                colorStops: [{ offset: 0, color: '#4F6169' },
                             { offset: 1, color: '#252E32'}]
            };

            $('#jqRadialGauge').jqRadialGauge({
				 title: {
        text: 'Chart Title',
        font: '40px sans-serif',
        lineWidth: 1,
        strokeStyle: 'black',
        fillStyle: 'yellow'
    },
                background: '#FFF',
                border: {
                    lineWidth: 6,
                    strokeStyle: '#418CF0',
                    padding: 16
                },
                shadows: {
                    enabled: true
                },
                anchor: {
                    visible: true,
                    fillStyle: anchorGradient,
                    radius: 0.10
                },
                tooltips: {
                    disabled: false,
                    highlighting: true
                },
                animation: {
                    duration: 1
                },
                scales: [
                         {
                             minimum: 0,
                             maximum: 150,
                             startAngle: 180,
                             endAngle: 360,
                             majorTickMarks: {
                                 length: 12,
                                 lineWidth: 2,
                                 interval: 10,
                                 offset: 0.84
                             },
                             minorTickMarks: {
                                 visible: true,
                                 length: 8,
                                 lineWidth: 2,
                                 interval: 2,
                                 offset: 0.84
                             },
                             labels: {
                                 orientation: 'horizontal',
                                 interval: 10,
                                 offset: 1.00
                             },
                             needles: [
                                        {
                                            value: 125,
                                            type: 'pointer',
                                            outerOffset: 0.8,
                                            mediumOffset: 0.7,
                                            width: 10,
                                            fillStyle: '#FF3366'
                                        }
                                      ],
                             ranges: [
                                        {
                                            outerOffset: 0.82,
                                            innerStartOffset: 0.76,
                                            innerEndOffset: 0.68,
                                            startValue: 00,
                                            endValue: 123,
                                            fillStyle: gradient1
                                        },
                                        
                                     ]
                         }
                        ]
            });

            $('#jqRadialGauge').bind('tooltipFormat', function (e, data) {

                var tooltip = '<b>BMW Bensville Vehicle Sold: ' + data.elementType + '</b> ' + '<br />';

                switch (data.elementType) {

                    case 'needle':
                        tooltip += 'Value: ' + data.value;
                        break;
                    case 'range':
                        tooltip += 'Start Value: ' + data.startValue + '<br/>End Value: ' + data.endValue;
                }

                return tooltip;
            });
        });
    </script>

 <script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: 'white' },
                             { offset: 1, color: 'white' }]
            };

            $('#jqChart').jqChart({
            	title: { text: 'Lead Status' },
               
                border: { strokeStyle: '#fff' },
                background: background,
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                    	type: 'doughnut',
                    	innerExtent: 0.5,
                    	outerExtent: 1.0,
                        fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382','#1A8B69'],
                        labels: {
                            stringFormat: '%.1f%%',
                            valueType: 'percentage',
                            font: '15px sans-serif',
                            fillStyle: 'white'
                        },
                        data: [['New', 4], ['Working', 6], ['Appointment', 13],
                               ['Quotation', 12], ['Lost', 3], ['Convert', 4], ['Unqualified', 6]]
                    }
                ]
            });
        });
    </script>
	<script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: 'white' },
                             { offset: 1, color: 'white' }]
            };

            $('#jqChart6').jqChart({
            	title: { text: 'Lead Count - Month to Date' },
               
                border: { strokeStyle: '#fff' },
                background: background,
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                    	type: 'doughnut',
                    	innerExtent: 0.5,
                    	outerExtent: 1.0,
                        fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382','#1A8B69'],
                        labels: {
                            stringFormat: '%.1f%%',
                            valueType: 'percentage',
                            font: '15px sans-serif',
                            fillStyle: 'white'
                        },
                        data: [['-', 13], ['Phone', 12], ['Internet', 12],
                               ['Walkin', 10],  ['Partner Referal', 15],['web', 4]]
                    }
                ]
            });
        });
    </script>
	
	
	<script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: 'white' },
                             { offset: 1, color: 'white' }]
            };

            $('#jqChart7').jqChart({
            	title: { text: 'Financial Information' },
               
                border: { strokeStyle: '#fff' },
                background: background,
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                    	type: 'doughnut',
                    	innerExtent: 0.5,
                    	outerExtent: 1.0,
                        fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382','#1A8B69'],
                        labels: {
                            stringFormat: '%.1f%%',
                            valueType: 'percentage',
                            font: '15px sans-serif',
                            fillStyle: 'white'
                        },
                        data: [['-', 8], ['New', 7], ['Appointment', 45],
                               ['Other', 6]]
                    }
                ]
            });
        });
    </script>
	<script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: 'white' },
                             { offset: 1, color: 'white' }]
            };

            $('#jqChart1').jqChart({
            	title: { text: 'Lead With Activity' },
               
                border: { strokeStyle: '#fff' },
                background: background,
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                    	type: 'doughnut',
                    	innerExtent: 0.5,
                    	outerExtent: 1.0,
                        fillStyles: '#1A3B69'['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF', '#1A3B69', '#FFE382','#1A5B69','#1A9B69'],
                        labels: {
                            stringFormat: '%.1f%%',
                            valueType: 'percentage',
                            font: '15px sans-serif',
                            fillStyle: 'white'
                        },
                        data: [['Allen Garry', 2], ['Jackson Nic', 1], ['John Thorpe', 2],
                               ['James Andrew', 2], ['Mike Any', 1], ['Williamson', 2],['John', 1],['Andrew', 2],['Marsh', 1],['Jason', 2]]
                    }
                ]
            });
        });
    </script>
	    <script lang="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#jqChart2').jqChart({
                title: { text: 'Lead Count Details' },
				 border: { strokeStyle: '#fff' },
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                        type: 'column',
                        title: 'Series 1',
                        fillStyles: ['#418CF0'],
                        data: [['BMW Bensville', 70]]
                    }
                ]
            });
        });
    </script>
	
	 <script lang="javascript" type="text/javascript">
        $(document).ready(function () {

            var background = {
                type: 'linearGradient',
                x0: 0,
                y0: 0,
                x1: 0,
                y1: 1,
                colorStops: [{ offset: 0, color: '#fff' },
                             { offset: 1, color: 'white' }]
            };

            $('#jqChart4').jqChart({
                title: { text: 'Monthly Sales Target' },
                legend: { title: 'Team' },
                border: { strokeStyle: '#fff' },
                background: background,
                animation: { duration: 1 },
                shadows: {
                    enabled: true
                },
                series: [
                    {
                        type: 'pie',
                        fillStyles: ['#418CF0', '#FCB441', '#E0400A', '#056492', '#BFBFBF'],
                        labels: {
                            stringFormat: '%.1f%%',
                            valueType: 'percentage',
                            font: '15px sans-serif',
                            fillStyle: 'white'
                        },
                        explodedRadius: 10,
                        explodedSlices: [5],
                        data: [['Austro ', 65], ['Cerner', 58], ['BMW Bensville', 30],
                               ]
                    }
                ]
            });

            $('#jqChart4').bind('tooltipFormat', function (e, data) {
                var percentage = data.series.getPercentage(data.value);
                percentage = data.chart.stringFormat(percentage, '%.2f%%');

                return '<b>' + data.dataItem[0] + '</b><br />' +
                       data.value + ' (' + percentage + ')';
            });
        });
    </script>

<script type="text/javascript">
 $(document).ready(function() {
   $('#example').DataTable();
} );
$('#example').dataTable( {
    "columnDefs": [ {
      "targets"  : 'no-sort',
      "orderable": false,
      "order": []
    }]
});

 $(document).ready(function() {
 var minheight=$( window ).height();

 
 $(".pagewrapper").css("min-height", minheight);
});
</script>
  <style>
 
   
  .panel-group .panel+.panel
  {
	   margin-top: 0px;
  }
  .panel-heading
  {
	    padding: 6px 15px;  
  }
  .form-group {
    margin-bottom: 5px;
}
.form-control {
   width:80%;
       font-size: 12px;
    height: 28px;
}
  label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 400;
}
  @media (min-width: 768px)
.modal-dialog {
    width: 9000px;
    margin: 30px auto;
}
  .navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:focus, .navbar-inverse .navbar-nav>.active>a:hover
  
  {
	     color: #fff;
    background-color: 
#4B5069; 
  }
  body {
	overflow-x: hidden;
   
font-family: 'Work Sans', sans-serif;
}
  .navbar
  {
  border-radius:0px;
  }
     .navbar-inverse {
     background: #1E2443;
    border-color: #1E2443;
}
.navbar-toggle
{
	float:left;
}

  </style>
</head>
<body>
<header>
  <div class="container-fluid" style="height:60px">
  <div class="row">
  <div class=" col-xs-6 col-sm-6">
<div class="navbar-header">
 
      <a class="navbar-brand" href="#"><img src="http://erpapps.com/img/logo-dark.png" width="80px"></a>
    </div>  </div>
	  <div class="col-sm-6 col-xs-6">
	
	<div class="dropdown  pull-right " style="margin-top: 20px;">
	  <img src="http://www.free-icons-download.net/images/man-icon-61345.png" width="30">
  <a class="  dropdown-toggle"  data-toggle="dropdown"><?php echo $this->session->userdata("dealer"); ?>
  <span class="caret"></span></a>
  <ul class="dropdown-menu zoomIn animated faster">
    <li><a href="#">Edit Profile</a></li>
    <li><a href="#">Logout</a></li>
   
  </ul>
</div>
</div>
	</div>
	</div>
		


<nav class="navbar navbar-inverse" >
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
       <li class="hidden-xs"><a href="#"> <span class="glyphicon glyphicon-home"></span></a></li>
<li class="<?php if(strtolower($this->uri->segment(1))=='home'){echo "active";} ?> hidden-sm hidden-md hidden-lg"><a href="#">Home</a></li>
       <li class="<?php if(strtolower($this->uri->segment(1))=='dashboard'){echo "active";} ?>" ><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
       <li class="<?php if(strtolower($this->uri->segment(1))=='lead'||$this->uri->segment(1)==''){echo "active";} ?>"><a href="<?php echo base_url('lead'); ?>">Lead</a></li>
<li class="<?php if(strtolower($this->uri->segment(1))=='stock'){echo "active";} ?>"><a href="<?php echo base_url('stock'); ?>">Vehicle Stock</a></li>
<!--
       <li><a href="#">Customer</a></li>
<li><a href="#">Driver</a></li>
       -->
     </ul>
     
    </div>
  </div>
</nav>