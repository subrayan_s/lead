<div class="container-fluid" >
   <div id="wait" class="loader"></div>
   <div id="displayResult"> </div>
   <div class="row" style="margin-bottom:20px;">
      <div class="col-sm-6">
         <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;"></span>  <span style="font-size:18px;">Vehicle Stock</span>
      </div>
      <div class="col-sm-6">
         <div class="pull-right">
            <button id="newStockItem" type="button" class="btn btn-default " data-toggle="modal" data-target="#myModal">New</button>&nbsp;
            <button type="button" class="btn btn-default" onClick="window.location.reload()">Refresh</button>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-12">
         <table class="table table-hover" id="example">
            <thead>
               <tr>
                  <th></th>
                  <th>Stock Number</th>
                  <th>VIN</th>
                  <th>REG No.</th>
                  <th>VehicleType</th>
                  <th>VehicleStockDays</th>
                  <th>Selling Dealer</th>
                  <th>Fuel Type</th>
                  <th>Make</th>
                  <th>Model</th>
                  <th>Edit</th>
               </tr>
            </thead>
            <tbody>
               <?php $no = 1;
                  foreach($stock_details as $new_stock_details)
                  {
                  
                  $id = $new_stock_details->id;
                  $stock = $new_stock_details->name;
                  $selling_dealer = $new_stock_details->selling_dealer__c;
                  $vehicle_type = $new_stock_details->vehicle_type__c;
                  $fuel_type = $new_stock_details->fuel_type__c;
                  $make = $new_stock_details->make__c;
                  $model = $new_stock_details->model__c;
                  ?>
               <tr>
                  <td><?php echo $no;  ?></td>
                  <td><a href="<?php echo WEB_DIR;?>stock/stock_view/<?php echo $id; ?>"><?php echo $stock;  ?></a></td>
                  <td><?=$new_stock_details->vin__c?></td>
                  <td><?=$new_stock_details->rego_no__c?></td>
                  <td><?php echo $vehicle_type;  ?></td>
                  <td><?=$new_stock_details->vehicle_stock_days__c?></td>
                  <td><?php echo $selling_dealer;  ?></td>
                  <td><?php echo $fuel_type;  ?></td>
                  <td><?php echo $make;  ?></td>
                  <td><?php echo $model;  ?></td>
                  <td>
                     <a class="openStockEdit" data-target="#myModal" data-toggle="modal" title="View Lead" data-id="<?php echo $id; ?>" href="#leadEdit"><span style="font-size:15px;" class="glyphicon glyphicon-edit"></span></a> 
                  </td>
               </tr>
               <?php $no++; } ?>
            </tbody>
         </table>
      </div>
   </div>
</div>

<!-- Insert Modal ------------------------------------> 
<div id="myModal" class="modal fade" role="dialog" >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="clearData close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center" id="stockTitle"></h4>
         </div>
         <form  id="stockForm" data-parsley-validate="" class="formCheck" method="post" accept-charset="utf-8">
            <div class="modal-body" style="padding: 5px;">
               <div style="margin-left:20px; margin-right:15px;">
                  <div class="panel panel-default">
                     <div class="panel-heading">Stock Information</div>
                     <div class="panel-body">
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Stock Number</label>
                                 <input type="text" name="name" class="form-control" id="name" required>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Model</label>
                                 <select class="form-control"name="model_name__c" id="model_name__c" class="form-control" required>
                                    <option value="">-- SELECT --</option>
                                    <option value="a007F00000V2wLTQAZ">M240I</option>
                                    <option value="a007F00000V30LSQAZ">225i</option>
                                    <option value="a007F00000V4LifQAF">LANDCRUISER</option>
                                    <option value="a007F00000q27nvQAA">Camry</option>
                                 </select>
                              </div>
                           </div>						   
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Selling Dealer</label>
                                 <select class="form-control"name="selling_dealer__c" id="selling_dealer__c" class="form-control">
                                    <option value="<?php echo $this->session->userdata('dealer'); ?>"><?php echo $this->session->userdata('dealer'); ?></option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">VIN</label>
                                 <input type="text" name="vin__c" class="form-control" id="vin__c">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Vehicle Type</label>
                                 <select class="form-control" name="vehicle_type__c" id="vehicle_type__c">
                                    <option value="">-- SELECT --</option>
                                    <option value="New"> New </option>
                                    <option value="Demo"> Demo </option>
                                    <option value="Used"> Used </option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Fuel Type</label>
                                 <input type="text" name="fuel_type__c" id="fuel_type__c" class="form-control" id="phone">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Reg No</label>
                                 <input type="text" name="rego_no__c" class="form-control" id="rego_no__c">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Manufacture Year</label>
                                 <input type="text" name="manufacture_year__c" id="manufacture_year__c" class="form-control">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Body Type</label>
                                 <input type="text" name="body_type__c" class="form-control" id="body_type__c">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Body Color</label>
                                 <input type="text" name="body_colour__c" class="form-control" id="body_colour__c">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Model</label>
                                 <input type="text" name="model__c" class="form-control" id="model__c">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Make</label>
                                 <input type="text" name="make__c" class="form-control" id="make__c">
                              </div>
                           </div>
                        </div>		
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>First Image Name</label>
                                 <input type="text" name="first_image_name__c" class="form-control" id="first_image_name__c">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Dealer Code</label>
                                 <input type="text" name="dealer_code__c" class="form-control" id="dealer_code__c">
                              </div>
                           </div>
                        </div>	
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Vehicle Stock Days</label>
                                 <input type="text" name="vehicle_stock_days__c" class="form-control" id="vehicle_stock_days__c">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Owner</label>
                                 <input type="text" name="ownerid" class="form-control" id="ownerid">
                              </div>
                           </div>
                        </div>							
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Price</label>
                                 <input type="text" name="egc_price__c" class="form-control" id="egc_price__c">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Series</label>
                                 <input type="text" name="series_c__c" class="form-control" id="series_c__c">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Stock Image</label>
                                 <input type="text" name="stock_image__c" class="form-control" id="stock_image__c">
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label>Stock Added Date</label>
                                 <input type="text"  class="form-control">
                              </div>
                           </div>
                        </div>	
					</div>
				</div>
				<div class="panel panel-default">
				 <div class="panel-heading"><a data-toggle="collapse" data-parent="#accordion_new" href="#addtionalInfo"><span class="glyphicon glyphicon-menu-down"></span></a>&nbsp;System Information</div>
				 <div id="addtionalInfo" class="panel-body collapse in" aria-expanded="true">				
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Creaed by</label>
								 
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Created date</label>
									<div id="createddate"></div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Last Modified by</label>
                               
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                              <div class="form-group">
                                 <label for="usr">Last modified date</label>
                              </div>
                           </div>
                        </div>
			  
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="clearData btn btn-default" data-dismiss="modal">Cancel</button>
				<input type="hidden" id="addOrEdit" />
				<input type="hidden" name="editID" id="editIDadd" />
               <input type="submit" class="stock_update btn btn-default btn-info" value="Save">
            </div>
         </form>
      </div>
   </div>
</div>
<script src="<?php echo WEB_DIR;?>assets/js/stock.js"></script>

<!-- Insert Modal Ends -->
