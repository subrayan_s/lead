<div class="panel panel-default">
      <div class="panel-heading" style="padding:5px 5px;" >
	  
	  <div class="col-sm-11">
	<span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;"></span>  <span style="font-size:18px;"><?php echo $details['make']; ?></span>
	  </div>
	      <div class="panel-body"></div>
</div>
</div>
<div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading"><h4>Vehicle Information</h4></div>
	       <div class="panel-body">
      <div class="col-sm-3">
<div class="form-group">
  <label><strong>Stock No:</strong></label>
 
      <p class="form-control-static"><?php echo $details['stockno']; ?></p>
    
</div>
<div class="form-group">
  <label><strong>Selling Dealer:</strong></label>
 
      <p class="form-control-static"><?php echo $details['sellingdealer']; ?></p>
    
</div>

</div>

<div class="col-sm-3">
<div class="form-group">
<label><strong>Vehicle Type :</strong></label>
 
      <p class="form-control-static"><?php echo $details['vehicletype']; ?></p>
    
</div>
<div class="form-group">
  <label><strong>Fuel Type: </strong></label>
 
      <p class="form-control-static"><?php echo $details['fueltype']; ?></p>
    
</div>



</div>
<div class="col-sm-3">
<div class="form-group">
<label><strong>Make:</strong></label>
 
      <p  class="form-control-static"><?php echo $details['make']; ?></p>
    
</div>
<div class="form-group">
<label><strong>Model:</strong></label>
 
      <p  class="form-control-static"><?php echo $details['model']; ?></p>
    
</div>


</div>

<div class="col-sm-3">

 <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    
	<?php
	
	if($details['imgname']!='')
	{
	?>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">  
        <img src="https://images.qrsolutions.com.au:9443/<?php echo $details['imgname']; ?>" alt="Los Angeles" style="width:100%;">
		
      </div>
<!--
      <div class="item">
        <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="New york" style="width:100%;">
      </div>
	  -->
    </div>
	<?php 
	}
	else{
	?>
	        <p> Image not available </p>
	
	<?php } ?>
	
	
	
	

   <!-- Left and right controls -->
    
	<!--
	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
	-->
  </div>
</div>
    </div>
    </div>

	
	 
	  </div>
	    </div>
	
