<div class="container-fluid">
   <div class="panel panel-default">
      <div class="panel-heading" style="padding:5px 5px;" >
         <div class="col-sm-6">
            <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;top:8px;"></span>  <span style="font-size:18px;"><?=$details[0]->make__c?></span>
         </div>
         <div class="col-sm-6">
            <div class=" row pull-right">
               <button type="button"  title="Details" id="btn1" class="btn btn-default btn-md">
               <span class="glyphicon glyphicon-th"></span> Details
               </button>
               <button  title="Activity" type="button" id="btn2" class="btn btn-default btn-md">
               <span class="glyphicon glyphicon-th-list"></span> Related
               </button>
            </div>
         </div>
         <div class="panel-body" style="padding:4px;"></div>
      </div>
   </div>
   <div class="panel-group relateddata">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>Vehicle Information</h4>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="col-md-9">
                  <div class="row">
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Stock No:</strong></label>
                           <span class="form-control-static"><?=$details[0]->name?></span>
                        </div>
                        <div class="form-group">
                           <label><strong>Selling Dealer:</strong></label>
                           <span class="form-control-static"><?=$details[0]->selling_dealer__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Vin:</strong></label>
                           <span class="form-control-static"><?=$details[0]->vin__c?></span>
                        </div>
                        <div class="form-group">
                           <label><strong>Reg No: </strong></label>
                           <span class="form-control-static"><?=$details[0]->rego_no__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Manafacture Year:</strong></label>
                           <span  class="form-control-static"><?=$details[0]->manufacture_year__c?></span>
                        </div>
                        <div class="form-group">
                           <label><strong>Body type:</strong></label>
                           <span class="form-control-static"><?=$details[0]->body_type__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Body Color:</strong></label>
                           <span class="form-control-static"><?=$details[0]->body_colour__c?></span>
                        </div>
                        <div class="form-group">
                           <label><strong>Price:</strong></label>
                           <span class="form-control-static"><?=$details[0]->egc_price__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Series:</strong></label>
                           <span class="form-control-static"><?=$details[0]->series_c__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Vehicle Type</strong></label>
                           <span class="form-control-static"><?=$details[0]->vehicle_type__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Fuel Type</strong></label>
                           <span class="form-control-static"><?=$details[0]->fuel_type__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Make</strong></label>
                           <span class="form-control-static"><?=$details[0]->make__c?></span>
                        </div>
                     </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                           <label><strong>Model</strong></label>
                           <span class="form-control-static"><?=$details[0]->model__c?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="row">
                     <div class="col-md-12">
                        <!-- Indicators -->
                        <?php
                           if($details[0]->first_image_name__c != '')
                           {
                           ?>
                        <img src="https://images.qrsolutions.com.au:9443/<?=$details[0]->first_image_name__c;?>" class="img-rounded" alt="Cinque Terre" width="250px" height="150px">
                     </div>
                     <?php 
                        }
                        else{
                        ?>
                     <img src=" <?php echo WEB_DIR;?>assets/images/Default_Image.jpg" class="img-rounded" alt="Cinque Terre" width="250px" height="150px">
                     <?php } ?>
                     <!-- Left and right controls -->
                     <!--
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                             <span class="glyphicon glyphicon-chevron-left"></span>
                             <span class="sr-only">Previous</span>
                           </a>
                           <a class="right carousel-control" href="#myCarousel" data-slide="next">
                             <span class="glyphicon glyphicon-chevron-right"></span>
                             <span class="sr-only">Next</span>
                           </a>
                        -->
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="panel panel-default">
         <div class="panel-heading">
            <h4>System Information</h4>
         </div>
         <div class="panel-body">
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Created by</strong></label>
                  <span  class="form-control-static"></span>
               </div>
               <div class="form-group">
                  <label><strong>Created Date</strong></label>
                  <span  class="form-control-static"><?=$details[0]->createddate?></span>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label><strong>Last modified by</strong></label>
                  <span  class="form-control-static"></span>
               </div>
               <div class="form-group">
                  <label><strong>Last modified date</strong></label>
                  <span  class="form-control-static"></span>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid activitydata">
   <div class="row">
      <div class="col-sm-8">
         <div class="panel-group">
            <div class="panel panel-default" style="margin-top: 10px;">
               <div class="panel-heading clearfix" style="background-color:#fff; border-bottom:1px  solid #ddd;">
                  <h4 class="panel-title pull-left" style="padding-top: 7.5px;">Attachment</h4>
                  <div class="btn-group pull-right">
                     <a href="#" class="btn btn-default btn-sm">Attachment</a>
                  </div>
               </div>
               <div class="panel-body">
               </div>
            </div>
            <div class="panel panel-default" style="margin-top: 10px;">
               <div class="panel-heading" style="background-color:#fff;border-bottom:1px  solid #ddd;">
                  <h4>Vehicle Stock History(1)</h4>
               </div>
               <div class="panel-body">
                  <table class="table table-condensed">
                     <thead>
                        <tr>
                           <th>Date</th>
                           <th>Field</th>
                           <th>User</th>
                           <th>Original value</th>
                           <th>New value</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>12/8/2018 9:38 AM</td>
                           <td>Created.</td>
                           <td>QRS Demo</td>
                           <td></td>
                           <td></td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>