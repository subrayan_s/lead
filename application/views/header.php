<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Salesforce</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/dataTables.bootstrap.css" />
      <link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/bootstrap.min.css" />
      <link rel="stylesheet" href="https://portal.erpapps.com/leadapp/css/animations.css">
      <script src="<?php echo WEB_DIR;?>assets/js/jquery.min.js"></script>
      <script src="<?php echo WEB_DIR;?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo WEB_DIR;?>assets/js/jquery.dataTables.min.js"></script> 
      <script src="<?php echo WEB_DIR;?>assets/js/dataTables.bootstrap.min.js"></script> 
      <script src="<?php echo WEB_DIR;?>assets/js/parsley.js"></script> 
      <link rel="stylesheet" href="<?php echo WEB_DIR;?>assets/css/basic.css" />
      <script src="<?php echo WEB_DIR;?>assets/js/autoComplete.js"></script> 
      <script type="text/javascript">
         $(document).ready(function() {
            $('#example').DataTable();
			let viewport = $( window ).height();
			$(".pagewrapper").css("min-height", viewport-120);
         });
      </script>
   </head>
   <body>
      <header>
      <div class="container-fluid" style="height:60px">
         <div class="row">
            <div class=" col-xs-6 col-sm-6">
               <div class="navbar-header">
                  <a class="navbar-brand" href="#"><img src="http://erpapps.com/img/logo-dark.png" width="80px"></a>
               </div>
            </div>
            <div class="col-sm-6 col-xs-6">
               <div class="dropdown  pull-right " style="margin-top: 20px;">
                  <img src="http://www.free-icons-download.net/images/man-icon-61345.png" width="30">
                  <a class="  dropdown-toggle"  data-toggle="dropdown"><?php echo $this->session->userdata("dealer"); ?>
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu zoomIn animated faster">
                     <li><a href="#">Edit Profile</a></li>
                     <li><a href="<?php echo base_url('login/logout'); ?>">Logout</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <nav class="navbar navbar-inverse">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>                        
               </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
               <ul class="nav navbar-nav">
                  <li class="hidden-xs"><a href="#"> <span class="glyphicon glyphicon-home"></span></a></li>
                  <li class="<?php if(strtolower($this->uri->segment(1))=='home'){echo "active";} ?> hidden-sm hidden-md hidden-lg"><a href="#">Home</a></li>
                  <li class="<?php if(strtolower($this->uri->segment(1))=='dashboard'){echo "active";} ?>" ><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
                  <li class="<?php if(strtolower($this->uri->segment(1))=='lead'||$this->uri->segment(1)==''){echo "active";} ?>"><a href="<?php echo base_url('lead'); ?>">Lead</a></li>
                  <li class="<?php if(strtolower($this->uri->segment(1))=='stock'){echo "active";} ?>"><a href="<?php echo base_url('stock'); ?>">Vehicle Stock</a></li>
               </ul>
            </div>
         </div>
      </nav>
      <div class="pagewrapper">