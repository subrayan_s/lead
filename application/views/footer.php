<footer class="page-footer font-small blue pt-4" style="border-top:1px solid #1E2443; ">
    <div class="footer-copyright text-center py-3">© 2018 All Rights Reserved.
      <a href="https://www.qrsolutions.com.au/"> QRSolutions</a>
    </div>
</footer>

<script>	
function loadDataTable(url){
	$('#example').DataTable({
        "pageLength" : 10,
		"bDestroy": true,
        "ajax": {
            url : url,
            type : 'GET',
        },
    });
}
	
$(document).on("click", ".open-leadEdit", function (){
	$('#editform').find('input:text').val('');  
	$("#modalDecide").html("Edit");
	$('#editform').find('input:selected').val('');  
	$("#addOrEdit").val('edit');
	$("#editIDadd").val($(this).attr('data-id'));	
	var leadid = $(this).data('id');
	$(".modal-body #lead_id").val(leadid);
	url = "<?=WEB_DIR.'/lead/leadeditFetch'?>";
	$("#wait").show();
	$.ajax({
		url: url,
		type: 'POST',
		data: { lead_id : leadid },
		dataType: 'json',
		cache: false,
		success: function (data){
		data = data[0];	
		console.log(data);
		$("#modalDecide").html(data.firstname + ' '+data.lastname)
		$.map(data, function(value, index) {
			$("#"+index).val(value);
		});	
		if(data.sent_to_dealer__c == 't')
			$("#data.sent_to_dealer__c").prop('checked',true);
		else
			$("#data.sent_to_dealer__c").prop('checked',true);
		if(data.eval_lead__c == 't')
			$("#data.eval_lead__c").prop('checked',true);
		else
			$("#data.eval_lead__c").prop('checked',true);
		if(data.appraisal_flag_field__c == 't')
			$("#data.appraisal_flag_field__c").prop('checked',true);
		else
			$("#data.appraisal_flag_field__c").prop('checked',true);

		if(data.introduction_to_finance_manager__c == 't')
			$("#data.introduction_to_finance_manager__c").prop('checked',true);
		else
			$("#data.introduction_to_finance_manager__c").prop('checked',true);
		
		
		$('#createdby').html('<b>'+data.createdbyid+'</b>');
		$('#lastmodifiedby').html('<b>'+data.lastmodifiedbyid+'</b>');
		$('#lastmodifieddate').html('<b>'+data.lastmodifieddate+'</b>');
		$('#createddate').html('<b>'+data.createddate+'</b>');
		
		getmodel();
		$("#wait").hide();
		}
	});
});

$(document).on("click", "#createNewLead", function (){
	$("#modalDecide").html("New Lead");
	$("#addOrEdit").val('add');
	$("#editIDadd").val(0);
});

$( document ).ready(function() {
   $(".activitydata").hide();

$( "#btn1" ).removeClass( "btn-default" );

$( "#btn1" ).addClass( "btn-primary" )
});

$( "#btn1" ).click(function() {
  $(".activitydata").hide();
   $(".relateddata").show();

$( "#btn2" ).removeClass( "btn-primary" );

$( "#btn2" ).addClass( "btn-default" )

$( "#btn1" ).removeClass( "btn-default" );

$( "#btn1" ).addClass( "btn-primary" )

});
$( "#btn2" ).click(function() {
   $(".activitydata").show();
   $(".relateddata").hide();
$( "#btn1" ).removeClass( "btn-primary" );

$( "#btn1" ).addClass( "btn-default" )

$( "#btn2" ).removeClass( "btn-default" );

$( "#btn2" ).addClass( "btn-primary" )
});
</script>
</body>
</html>