<div class="panel panel-default">
  <div class="panel-heading" style="padding:5px 5px;" >	  
	  <div class="col-sm-11">
	    <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;"></span>  <span style="font-size:18px;"><?php echo $details['rego']; ?></span>
	  </div>
	  <div class="panel-body"></div>
  </div>
</div>
<div class="panel-group">
  <div class="panel-heading"><h4>Vehicle Information</h4></div>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Details</a></li>
    <li><a data-toggle="tab" href="#menu1">Related</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="panel panel-default">        

         <div class="panel-body">            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo 'Dealership Name'; //echo $details['servicedealername']; ?></span>                
              </div>
          </div>
          <br>
          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Rego No:</strong></label>
                <p class="form-control-static"><?php echo $details['rego']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Vin:</strong></label> 
                  <p class="form-control-static"><?php echo $details['vin']; ?></p>    
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Selling Dealer:</strong></label>
                <p class="form-control-static"><?php echo $details['selling']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Delivery Date:</strong></label> 
                  <p class="form-control-static"><?php echo $details['delivery']; ?></p>    
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Created date:</strong></label>
                <p class="form-control-static"><?php echo $details['created']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Status:</strong></label> 
                  <p class="form-control-static"><?php echo $details['status']; ?></p>    
            </div>            
          </div>                  
      </div>

      <div class="panel-body">

            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo 'Recently Viewed'; ?></span>                
              </div>
          </div>
          <br>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Rego No:</strong></label>
                <p class="form-control-static"><?php echo $details['rego']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Vin:</strong></label> 
                  <p class="form-control-static"><?php echo $details['vin']; ?></p>    
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Selling Dealer:</strong></label>
                <p class="form-control-static"><?php echo $details['selling']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Delivery Date:</strong></label> 
                  <p class="form-control-static"><?php echo $details['delivery']; ?></p>    
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Created date:</strong></label>
                <p class="form-control-static"><?php echo $details['created']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Status:</strong></label> 
                  <p class="form-control-static"><?php echo $details['status']; ?></p>    
            </div>            
          </div>          
      </div>     

    </div>

    </div>
    <div id="menu1" class="tab-pane fade">

      <div class="panel-body">            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo 'Vehicle Ownership'; ?></span>                
              </div>
          </div>
          <br>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Date:</strong></label>
                <p class="form-control-static"><?php //echo $details['vin']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Field:</strong></label> 
                  <p class="form-control-static"><?php //echo $details['make']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>User:</strong></label>
                <p class="form-control-static"><?php //echo $details['model']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Original Value:</strong></label> 
                  <p class="form-control-static"><?php// echo $details['vehicletype']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>New Value:</strong></label>
                <p class="form-control-static"><?php// echo '----'; ?></p>                
            </div>                   
          </div>          
      </div>   

      
    </div>
  </div>    
</div>

	
