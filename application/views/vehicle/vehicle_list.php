<div class="container-fluid" >
  <div id="wait" class="loader"></div>
  <div class="row" style="margin-bottom:20px;">
    <div class="col-sm-6">
      <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;"></span>  <span style="font-size:18px;">Vehicle </span>
    </div>
    <div class="col-sm-6">
      <div class="pull-right">
        <button type="button" class="btn btn-default " data-toggle="modal" data-target="#myModal">New</button>&nbsp;
        <button type="button" class="btn btn-default ">Refresh</button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover" id="example">
        <thead>
          <tr>
            <th></th>
            <th>Vin##</th>
            <th>Selling Dealer</th>
            <th>Rego Number</th>
            <th>Make</th>
            <th>Model</th>
            <th>Model Name</th>
            <th>Stock No</th>
            <th>Vehicle type</th>
            <th>Service Dealership Name</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
            foreach($vehicle_details as $new_vehicle_details)
            {
            
            $id = $new_vehicle_details->id;
            $vin = $new_vehicle_details->name;
            $selling_dealer = $new_vehicle_details->selling_dealer__c;
            $regonumber = $new_vehicle_details->rego_number__c;
            $make = $new_vehicle_details->make__c;
            //$model = $new_vehicle_details->model_description__c;
            $modelname = $new_vehicle_details->model_name__c;
            $stock = $new_vehicle_details->stock_no__c;
            $vehicle_type = $new_vehicle_details->vehicle_type__c;
            $servicedealershipname = $new_vehicle_details->service_dealership_name__c;
           
            ?>
          <tr>
            <td><?php echo $no;  ?></td>
            <td><a href="<?php echo WEB_DIR;?>vehicle/vehicle_view/<?php echo $id; ?>"><?php echo $vin;  ?></a></td>
            <td><?php echo $selling_dealer;  ?></td>
            <td><?php echo $regonumber; ?></td>
            <td><?php echo $make;  ?></td>
            <td><?php// echo $model;  ?></td>
            <td><?php echo $modelname;  ?></td>
            <td><?php echo $stock;  ?></td>
            <td><?php echo $vehicle_type;  ?></td>
			<td><?php echo $servicedealershipname;  ?></td>
            <td>
              <a data-toggle="modal" title="View Lead" data-id="<?php echo $id; ?>" class="open-vehicleEdit" onclick="edit_vehicle(<?php echo $id;?>)"><span style="font-size:15px;" class="glyphicon glyphicon-edit"></span></a> 
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- Insert Modal --> 
<div id="myModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">New Vehicle</h4>
      </div>
      <form  data-parsley-validate="" class="formCheck" action="<?php echo WEB_DIR;?>vehicle/vehicle_insert" method="post" accept-charset="utf-8">
        <div class="modal-body" style="padding: 5px;">
          <div style="margin-left:20px; margin-right:15px;">
            <div class="panel panel-default">
              <div class="panel-heading">Vehicle Information</div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="usr">VIN#</label>
                      <input type="text" name="name" class="form-control" id="name">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Selling Dealer">Selling Dealer</label>
                      <select class="form-control" name="selling_dealer__c" id="selling_dealer__c" required="">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>

                    </div>
                  </div>              
                </div>

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Rego Number">Rego Number</label>
                      <input class="form-control" name="rego_number__c" id="rego_number__c" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Make">Make</label>
                      <select class="form-control" name="make__c" id="make__c" required="">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label>Model</label>
                      <input type="text" name="model__c" id="model__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="usr">Model Name</label>
                      <input type="text" name="model_name__c" class="form-control" id="model_name__c">
                    </div>
                  </div>
                </div>

				<div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label>Stock No</label>
                      <input type="text" name="stock_no__c" id="stock_no__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Vehicle type">Vehicle type</label>
                      <select class="form-control" name="vehicle_type__c" id="vehicle_type__c" required="">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Service Dealership Name">Service Dealership Name</label>
                      <select class="form-control" name="service_dealership_name__c" id="service_dealership_name__c" required="">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>
                    </div>
                  </div>  
				</div>                              

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Save & New</button> -->
          <input type="submit" class="btn btn-default btn-info" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Insert Modal Ends -->
<!-- Edit Modal -->
<div id="modal_edit_vehicle" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Edit Vehicle</h4>
      </div>
      <form  data-parsley-validate="" class="formCheck" id="editform" action="<?php echo WEB_DIR;?>vehicle/vehicle_edit_insert" method="post" accept-charset="utf-8">
        <div class="modal-body" style="padding: 5px;">
          <div style="margin-left:20px; margin-right:15px;">
            <div class="panel panel-default">
              <div class="panel-heading">Stock Information</div>
              <div class="panel-body">

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="usr">VIN#</label>
                      <input type="text" name="edit_name" class="form-control" id="edit_name" required="required">
                      <input type="hidden" id="edit_id" name="edit_id" >                    
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Selling Dealer">Selling Dealer</label>
                      <select class="form-control" name="edit_selling_dealer__c" id="edit_selling_dealer__c">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>

                    </div>
                  </div>              
                </div>

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Rego Number">Rego Number</label>
                      <input class="form-control" name="edit_rego_number__c" id="edit_rego_number__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Make">Make</label>
                      <select class="form-control" name="edit_make__c" id="edit_make__c">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label>Model</label>
                      <input type="text" name="edit_model__c" id="edit_model__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="usr">Model Name</label>
                      <input type="text" name="edit_model_name__c" class="form-control" id="edit_model_name__c">
                    </div>
                  </div>
                </div>

				<div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label>Stock No</label>
                      <input type="text" name="edit_stock_no__c" id="edit_stock_no__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Vehicle type">Vehicle type</label>
                      <select class="form-control" name="edit_vehicle_type__c" id="edit_vehicle_type__c">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Service Dealership Name">Service Dealership Name</label>
                      <select class="form-control" name="edit_service_dealership_name__c" id="edit_service_dealership_name__c">
                        <option value="">-- SELECT --</option>
                        <option value="New"> New </option>
                        <option value="Demo"> Demo </option>
                        <option value="Used"> Used </option>
                      </select>
                    </div>
                  </div>  
				</div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Save & New</button> -->
          <input type="submit" class="btn btn-default btn-info" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit Modal Ends -->

<script type="text/javascript">
function edit_vehicle(id)
{
	// save_method = 'update';
    $('#editform')[0].reset(); // reset form on modals
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo WEB_DIR; ?>"+"vehicle/ajax_edit/"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
        	
        	$("#edit_id").val(data[0].id);
        	$("#edit_name").val(data[0].name);
            $('#edit_selling_dealer__c').val(data[0].selling_dealer__c);
            $('#edit_rego_number__c').val(data[0].rego_number__c);
            $('#edit_make__c').val(data[0].make__c);
            $('#edit_model__c').val(data[0].model__c);
 			$('#edit_model_name__c').val(data[0].model_name__c);
 			$('#edit_stock_no__c').val(data[0].stock_no__c);
 			$('#edit_vehicle_type__c').val(data[0].vehicle_type__c); 			
 			$('#edit_service_dealership_name__c').val(data[0].service_dealership_name__c); 			
 
            $('#modal_edit_vehicle').modal('show'); // show bootstrap modal when complete loaded
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
</script>