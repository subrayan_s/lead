<div class="container-fluid" >
  <div id="wait" class="loader"></div>
  <div class="row" style="margin-bottom:20px;">
    <div class="col-sm-6">
      <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;"></span>  <span style="font-size:18px;">Vehicle Ownership </span>
    </div>
    <div class="col-sm-6">
      <div class="pull-right">
        <button type="button" class="btn btn-default " data-toggle="modal" data-target="#myModal">New</button>&nbsp;
        <button type="button" class="btn btn-default ">Refresh</button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-hover" id="example">
        <thead>
          <tr>
            <th>Rego NO</th>
            <th>Contact Name</th>
            <th>Account Name</th>
            <th>Status</th>
            <th>Start date</th>
            <th>Delivery Date</th>
            <th>End Date</th>
            <th>RRP</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $no = 1;
            //echo '<pre>'; print_r($vehicle_ownership_details );
            foreach($vehicle_ownership_details as $new_vehicle_ownership_details)
            {         
              $id          =  $new_vehicle_ownership_details->id;
              $rego        = $new_vehicle_ownership_details->name;
              $contactName = $new_vehicle_ownership_details->contactname__c;
              $account     = $new_vehicle_ownership_details->account_name__c;
              $status      = $new_vehicle_ownership_details->status__c;
              $startDate   = $new_vehicle_ownership_details->start_date__c;
              $endDate     = $new_vehicle_ownership_details->end_date__c;
              $deliveryDate= $new_vehicle_ownership_details->delivery_date__c;
              $rrp         = $new_vehicle_ownership_details->rrp__c;
            ?>
          <tr>
            <td><?php echo $rego;  ?></td>
            <td>
              <a href="<?php echo WEB_DIR;?>vehicle/vehicle_ownership_view/<?php echo $id; ?>"><?php echo $contactName;  ?></a>
            </td>
            <td><?php echo $account;  ?></td>
            <td><?php echo $status; ?></td>
            <td><?php echo $startDate;  ?></td>
            <td><?php echo $deliveryDate;  ?></td>
            <td><?php echo $endDate;  ?></td>
            <td><?php echo $rrp;  ?></td>
            <td>
              <a data-toggle="modal" title="View Lead" data-id="<?php echo $id; ?>" class="open-vehicleEdit" onclick="edit_vehicle_ownership(<?php echo $id;?>)"><span style="font-size:15px;" class="glyphicon glyphicon-edit"></span></a> 
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- Insert Modal --> 
<div id="myModal" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">New Vehicle Ownership</h4>
      </div>
      <form  data-parsley-validate="" class="formCheck" action="<?php echo WEB_DIR;?>vehicle/ownership_insert" method="post" accept-charset="utf-8">
        <div class="modal-body" style="padding: 5px;">
          <div style="margin-left:20px; margin-right:15px;">
            <div class="panel panel-default">
              <div class="panel-heading">Vehicle Ownership Information</div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Rego NO">Rego NO</label>
                      <input type="text" name="name" class="form-control" id="name" required="required">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Contact Name">Contact Name</label>
                      <input type="text" name="contactname__c" class="form-control" id="contactname__c" required="required">
                    </div>
                  </div>                              
                </div>

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Account Name">Account Name</label>
                      <input class="form-control" name="account_name__c" id="account_name__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Status">Status</label>
                      <select class="form-control" name="status__c" id="status__c">
                        <option value="">-- SELECT --</option>
                        <option value="Active"> Active </option>
                        <option value="Inactive"> Inactive </option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Start date">Start date</label>
                      <input type="text" name="start_date__c" id="start_date__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Delivery Date">Delivery Date</label>
                      <input type="text" name="delivery_date__c" class="form-control" id="delivery_date__c">
                    </div>
                  </div>
                </div>

                <div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="End Date">End Date</label>
                      <input type="text" name="end_date__c" id="end_date__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="RRP">RRP</label>
                      <input type="text" name="rrp__c" id="rrp__c" class="form-control">
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Save & New</button> -->
          <input type="submit" class="btn btn-default btn-info" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Insert Modal Ends -->
<!-- Edit Modal -->
<div id="modal_edit_vehicle" class="modal fade" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Edit Vehicle Ownership</h4>
      </div>
      <form  data-parsley-validate="" class="formCheck" id="editform" action="<?php echo WEB_DIR;?>vehicle/vehicle_ownership_edit_insert" method="post" accept-charset="utf-8">
        <div class="modal-body" style="padding: 5px;">
          <div style="margin-left:20px; margin-right:15px;">
            <div class="panel panel-default">
              <div class="panel-heading">Vehicle Ownership Information</div>
              <div class="panel-body">

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Rego NO">Rego NO</label>
                      <input type="text" name="edit_name" class="form-control" id="edit_name" required="required">
                      <input type="hidden" id="edit_id" name="edit_id" >
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Contact Name">Contact Name</label>
                      <input type="text" name="edit_contactname__c" class="form-control" id="edit_contactname__c" required="required">
                    </div>
                  </div>                              
                </div>

                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Account Name">Account Name</label>
                      <input class="form-control" name="edit_account_name__c" id="edit_account_name__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Status">Status</label>
                      <select class="form-control" name="edit_status__c" id="edit_status__c">
                        <option value="">-- SELECT --</option>
                        <option value="Active"> Active </option>
                        <option value="Inactive"> Inactive </option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Start date">Start date</label>
                      <input type="text" name="edit_start_date__c" id="edit_start_date__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="Delivery Date">Delivery Date</label>
                      <input type="text" name="edit_delivery_date__c" class="form-control" id="edit_delivery_date__c">
                    </div>
                  </div>
                </div>

                <div class="row">                  
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="End Date">End Date</label>
                      <input type="text" name="edit_end_date__c" id="edit_end_date__c" class="form-control">
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 1px;">
                    <div class="form-group">
                      <label for="RRP">RRP</label>
                      <input type="text" name="edit_rrp__c" id="edit_rrp__c" class="form-control">
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Save & New</button> -->
          <input type="submit" class="btn btn-default btn-info" value="Save">
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit Modal Ends -->


<script type="text/javascript">

//****for add modal*****//

$('#start_date__c').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});

$('#end_date__c').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});

$('#delivery_date__c').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});



//****for edit modal*****//

$('#edit_start_date__c').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});

$('#edit_end_date__c').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});

$('#edit_delivery_date__c').datepicker({
    format: 'yyyy-mm-dd',
    startDate: '-3d'
});

function edit_vehicle_ownership(id)
{
  // save_method = 'update';
    $('#editform')[0].reset(); // reset form on modals
 
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo WEB_DIR; ?>"+"vehicle/ajax_edit_ownership/"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          
          $("#edit_id").val(data[0].id);
          $("#edit_name").val(data[0].name);
          $('#edit_contactname__c').val(data[0].contactname__c);
          $('#edit_account_name__c').val(data[0].account_name__c);
          $('#edit_status__c').val(data[0].status__c);
          $('#edit_start_date__c').val(data[0].start_date__c);
          $('#edit_delivery_date__c').val(data[0].delivery_date__c);
          $('#edit_end_date__c').val(data[0].end_date__c);
          $('#edit_rrp__c').val(data[0].rrp__c);      
          
 
          $('#modal_edit_vehicle').modal('show'); // show bootstrap modal when complete loaded
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
</script>