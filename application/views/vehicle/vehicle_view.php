<div class="panel panel-default">
  <div class="panel-heading" style="padding:5px 5px;" >	  
	  <div class="col-sm-11">
	    <span class="glyphicon glyphicon-book" style="color:#F1595E;font-size:25px;"></span>  <span style="font-size:18px;"><?php echo $details['make']; ?></span>
	  </div>
	  <div class="panel-body"></div>
  </div>
</div>
<div class="panel-group">
  <div class="panel-heading"><h4>Vehicle Information</h4></div>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Details</a></li>
    <li><a data-toggle="tab" href="#menu1">Related</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="panel panel-default">        

         <div class="panel-body">

            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo $details['servicedealername']; ?></span>                
              </div>
          </div>
          <br>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>VIN:</strong></label>
                <p class="form-control-static"><?php echo $details['vin']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Make:</strong></label> 
                  <p class="form-control-static"><?php echo $details['make']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Model:</strong></label>
                <p class="form-control-static"><?php echo $details['model']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Vehicle Type:</strong></label> 
                  <p class="form-control-static"><?php echo $details['vehicletype']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Model Description:</strong></label>
                <p class="form-control-static"><?php echo '----'; ?></p>                
            </div>
            
          </div>

          <div class="col-sm-3">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Los Angeles" style="width:100%;">
                  </div>

                  <div class="item">
                    <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="Chicago" style="width:100%;">
                  </div>
                
                  <div class="item">
                    <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="New york" style="width:100%;">
                  </div>
                </div>

               <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>
          </div>          
      </div>

      <div class="panel-body">

            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo 'Recently Viewed';//echo $details['servicedealername']; ?></span>                
              </div>
          </div>
          <br>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>VIN:</strong></label>
                <p class="form-control-static"><?php echo $details['vin']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Make:</strong></label> 
                  <p class="form-control-static"><?php echo $details['make']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Model:</strong></label>
                <p class="form-control-static"><?php echo $details['model']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Vehicle Type:</strong></label> 
                  <p class="form-control-static"><?php echo $details['vehicletype']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Model Description:</strong></label>
                <p class="form-control-static"><?php echo '----'; ?></p>                
            </div>            
          </div>          
      </div>
      

    </div>

    </div>
    <div id="menu1" class="tab-pane fade">

      <div class="panel-body">            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo 'Vehicle Ownership'; ?></span>                
              </div>
          </div>
          <br>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Gego NO:</strong></label>
                <p class="form-control-static"><?php //echo $details['vin']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Vin number:</strong></label> 
                  <p class="form-control-static"><?php //echo $details['make']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Contact Name:</strong></label>
                <p class="form-control-static"><?php //echo $details['model']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Start date:</strong></label> 
                  <p class="form-control-static"><?php// echo $details['vehicletype']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>End date:</strong></label>
                <p class="form-control-static"><?php// echo '----'; ?></p>                
            </div>     
            <div class="form-group">
              <label><strong>status:</strong></label>
                <p class="form-control-static"><?php //echo '----'; ?></p>                
            </div>       
          </div>          
      </div>

      <div class="panel-body">            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo 'Service repair Order'; ?></span>                
              </div>
          </div>
          <br>
          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Repair Order number:</strong></label>
                <p class="form-control-static"><?php //echo $details['vin']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Vin:</strong></label> 
                  <p class="form-control-static"><?php //echo $details['make']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Name:</strong></label>
                <p class="form-control-static"><?php //echo $details['model']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Invoice Date:</strong></label> 
                  <p class="form-control-static"><?php// echo $details['vehicletype']; ?></p>    
            </div>
          </div>        
      </div>

      <div class="panel-body">            
          <div class="panel panel-default">
              <div class="panel-heading" style="padding:5px 5px;">                
                  <span style="font-size:18px;"> &nbsp;<?php echo 'Vehicle History'; ?></span>                
              </div>
          </div>
          <br>
          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>Date:</strong></label>
                <p class="form-control-static"><?php //echo $details['vin']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Field:</strong></label> 
                  <p class="form-control-static"><?php //echo $details['make']; ?></p>    
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>User:</strong></label>
                <p class="form-control-static"><?php //echo $details['model']; ?></p>                
            </div>
            <div class="form-group">
              <label><strong>Original Value:</strong></label> 
                  <p class="form-control-static"><?php// echo $details['vehicletype']; ?></p>    
            </div>
          </div> 

          <div class="col-sm-3">
            <div class="form-group">
              <label><strong>New Value:</strong></label>
                <p class="form-control-static"><?php //echo $details['model']; ?></p>                
            </div>            
          </div> 
               
      </div> 
    </div>
  </div>    
</div>

	
