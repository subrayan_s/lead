 <!-- views/vehicle/add.php-->
 <div class="page-wrapper">
           
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Add Vehicle</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Vehicle</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
          
<div class="row"> 
                    <div class="col-12">
                        <div class="card">
     
                            <form class="form-horizontal" method="post" action="<?php echo WEB_DIR;?>lead/lead_forms">
                                <div class="card-body">
                                    <h4 class="card-title">Add Vehicle</h4>
                                    <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="fname2" class="col-sm-3 text-right control-label col-form-label">VIN</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="firstname" id="fname2" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="lname2" class="col-sm-3 text-right control-label col-form-label">Owner</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="lastname" id="lname2" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Selling Dealer</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="company" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Vehicle Type</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="mobile" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
										</div>
										
										<div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Rego Number</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Salesman Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Make</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Dealer Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Make Code</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Service Dealership Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Model</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Colour</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Fuel Type</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Manufacturer Year</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Engine Number</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Number of Seats</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Model Name</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Num of Cylinder</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                        
                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Stock No</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Num of Doors</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Price</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Num of Gears</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Model Car Description</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Body Colour</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Engine Capacity</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Body Type</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                                     <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="uname1" class="col-sm-3 text-right control-label col-form-label">Warantee Expiry Date</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" id="uname1" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="form-group row">
                                                <label for="nname" class="col-sm-3 text-right control-label col-form-label">Gear Box</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="requestedtype" id="nname" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                     </div>

                              
                                <div class="card-body">
                                    <div class="form-group m-b-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                        <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
           