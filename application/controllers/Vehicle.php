<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vehicle extends CI_Controller {
	public function __construct()
	{
		parent ::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Common_model');
	}
	public function index()
	{ 
		$data['vehicle_details'] = $this->Common_model->all_record_result('salesforce.vehicle__c');
		$this->load->view('header');
		$this->load->view('vehicle/vehicle_list',$data);
		$this->load->view('stock_footer');
	}
    public function vehicle_view()
    {
		$id = $this->uri->segment('3');
	    $where = array('id'=>$id);
		$data['vehicle_details'] = $this->Common_model->all_record_result('salesforce.vehicle__c', $where);
		$vin = $data['vehicle_details'][0]->name; 
		$sellingdealer = $data['vehicle_details'][0]->selling_dealer__c;
		$regonumber = $data['vehicle_details'][0]->rego_number__c;
		$make = $data['vehicle_details'][0]->make__c;
		$model = $data['vehicle_details'][0]->model__c;
		$modelname = $data['vehicle_details'][0]->model_name__c;
		$stockno = $data['vehicle_details'][0]->stock_no__c;
		$vehicletype = $data['vehicle_details'][0]->vehicle_type__c;
		$servicedealername = $data['vehicle_details'][0]->service_dealership_name__c;
		
		$data['details'] = array('stockno'=> $stockno, 'sellingdealer'=> $sellingdealer, 'vehicletype'=> $vehicletype, 'vin'=> $vin , 'make'=>$make, 'model'=> $model, 'servicedealername' => $servicedealername,'regonumber' => $regonumber,'modelname'=> $modelname);

		    $this->load->view('header');
			$this->load->view('vehicle/vehicle_view', $data);
		    $this->load->view('footer');
    }



public function vehicle_insert()
{
    $name=$this->input->post('name'); 
	$model_name__c=$this->input->post('model_name__c');
	$make__c=$this->input->post('make__c');
	$vehicle_type__c=$this->input->post('vehicle_type__c');
	$rego_number__c=$this->input->post('rego_number__c');
	$service_dealership_name__c=$this->input->post('service_dealership_name__c');
	$stock_no__c=$this->input->post('stock_no__c'); 
	$model__c=$this->input->post('model__c');
	$selling_dealer__c=$this->input->post('selling_dealer__c');
	$isdeleted ='f';
	$systemmodstamp = date('Y-m-d H:i:s');
	$createddate = date('Y-m-d H:i:s');

    $data = array(
    		'name'                       => $name,
			'model_name__c'              => $model_name__c,
			'make__c'                    => $make__c,
			'vehicle_type__c'            => $vehicle_type__c,
			'rego_number__c'             => $rego_number__c,
			'service_dealership_name__c' => $service_dealership_name__c,
			'stock_no__c'                => $stock_no__c,
			'model__c'                   => $model__c,
			'selling_dealer__c'          => $selling_dealer__c,
			'isdeleted'                  => $isdeleted,
			'systemmodstamp'			 => $systemmodstamp,
			'createddate'			     => $createddate			
	);

    $return_value = $this->Common_model->insertVal('salesforce.vehicle__c',$data);
	redirect('vehicle');
}


public function ajax_edit($id)
{
	$where = array('id'=>$id);
	$data = $this->Common_model->all_record_result('salesforce.vehicle__c', $where);
 	echo json_encode($data);
}

public function vehicle_edit_insert()
{
	$id=$this->input->post('edit_id'); 
    $name=$this->input->post('edit_name'); 
	$model_name__c=$this->input->post('edit_model_name__c');
	$make__c=$this->input->post('edit_make__c');
	$vehicle_type__c=$this->input->post('edit_vehicle_type__c');
	$rego_number__c=$this->input->post('edit_rego_number__c');
	$service_dealership_name__c=$this->input->post('edit_service_dealership_name__c');
	$stock_no__c=$this->input->post('edit_stock_no__c'); 
	$model__c=$this->input->post('edit_model__c');
	$selling_dealer__c=$this->input->post('edit_selling_dealer__c');
	$isdeleted ='f';
	$systemmodstamp = date('Y-m-d H:i:s');
	$createddate = date('Y-m-d H:i:s');

    $data = array(
    		'name'                       => $name,
			'model_name__c'              => $model_name__c,
			'make__c'                    => $make__c,
			'vehicle_type__c'            => $vehicle_type__c,
			'rego_number__c'             => $rego_number__c,
			'service_dealership_name__c' => $service_dealership_name__c,
			'stock_no__c'                => $stock_no__c,
			'model__c'                   => $model__c,
			'selling_dealer__c'          => $selling_dealer__c,
			'isdeleted'                  => $isdeleted,
			'systemmodstamp'			 => $systemmodstamp,
			'createddate'			     => $createddate			
	        );

    $where = array('id'=>$id);
    $return_value = $this->Common_model->record_update('salesforce.vehicle__c', $where, $data);
	redirect('vehicle');
}


    public function ownership()
    { 
		$data['vehicle_ownership_details'] = $this->Common_model->all_record_result('salesforce.vehicle_ownership__c');
		$this->load->view('header');
		$this->load->view('vehicle/vehicle_ownership_list',$data);
		$this->load->view('stock_footer');
	}

	
	public function vehicle_ownership_view()
    {
		$id = $this->uri->segment('3');
	    $where = array('id'=>$id);
		$data['vehicle_ownership_details'] = $this->Common_model->all_record_result('salesforce.vehicle_ownership__c', $where);

		$rego     = $data['vehicle_ownership_details'][0]->name; 
		$vin      = '';
		$created  = $data['vehicle_ownership_details'][0]->createddate;
		$delivery = $data['vehicle_ownership_details'][0]->delivery_date__c;
		$status   = $data['vehicle_ownership_details'][0]->status__c;
		$selling  = '';

		$data['details'] = array('vin'=> $vin , 'rego'=>$rego,'created'=>$created,'delivery'=>$delivery,'status'=>$status, 'selling'=>$selling);
		    $this->load->view('header');
			$this->load->view('vehicle/vehicle_ownership_view', $data);
		    $this->load->view('footer');
    }

	public function ownership_insert()
    { 
    	$rego        = $this->input->post('name');
        $contactName = $this->input->post('contactname__c'); 
        $account     = $this->input->post('account_name__c');
        $status      = $this->input->post('status__c'); 
        $startDate   = $this->input->post('start_date__c'); 
        $endDate     = $this->input->post('end_date__c');
        $deliveryDate= $this->input->post('delivery_date__c');
        $rrp         = $this->input->post('rrp__c'); 
        $isdeleted   = 'f';
        $systemmodstamp = date('Y-m-d H:i:s');
        $createddate = date('Y-m-d H:i:s');

    	$data = array(
    		'name'               => $rego,
			'contactname__c'     => $contactName,
			'account_name__c'    => $account,
			'status__c'          => $status,
			'start_date__c'      => $startDate,
			'delivery_date__c'   => $deliveryDate,
			'end_date__c'        => $endDate,
			'rrp__c'             => $rrp,
			'isdeleted'			 => $isdeleted,
			'systemmodstamp'     => $systemmodstamp,
			'createddate'		 => $createddate			
	    );

    	$return_value = $this->Common_model->insertVal('salesforce.vehicle_ownership__c',$data);
		redirect('vehicle/ownership');		
	}

	public function ajax_edit_ownership($id)
	{
		$where = array('id'=>$id);
		$data = $this->Common_model->all_record_result('salesforce.vehicle_ownership__c', $where);
	 	echo json_encode($data);
	}	

	public function vehicle_ownership_edit_insert()
	{
		$id          = $this->input->post('edit_id');
		$rego        = $this->input->post('edit_name');
        $contactName = $this->input->post('edit_contactname__c'); 
        $account     = $this->input->post('edit_account_name__c');
        $status      = $this->input->post('edit_status__c'); 
        $startDate   = $this->input->post('edit_start_date__c'); 
        $endDate     = $this->input->post('edit_end_date__c');
        $deliveryDate= $this->input->post('edit_delivery_date__c');
        $rrp         = $this->input->post('edit_rrp__c'); 
        $isdeleted   = 'f';
        $systemmodstamp = date('Y-m-d H:i:s');
        
    	$data = array(
    		'name'               => $rego,
			'contactname__c'     => $contactName,
			'account_name__c'    => $account,
			'status__c'          => $status,
			'start_date__c'      => $startDate,
			'delivery_date__c'   => $deliveryDate,
			'end_date__c'        => $endDate,
			'rrp__c'             => $rrp,
			'isdeleted'			 => $isdeleted,
			'systemmodstamp'     => $systemmodstamp,
		);

	    $where = array('id'=>$id);
	    $return_value = $this->Common_model->record_update('salesforce.vehicle_ownership__c', $where, $data);
		redirect('vehicle/ownership');
	}





}