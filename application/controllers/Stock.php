<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

  public function __construct(){
		parent ::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Common_model');
		if($this->session->userdata("dealer")=='')
		 redirect(base_url());
	}

	public function index(){
		$dealer = $this->session->userdata("dealer");
		$where = array('selling_dealer__c'=>$dealer);
		$data['stock_details'] = $this->Common_model->all_record_result('salesforce.vehicle_stock__c', $where,'id');
		$this->load->view('header');
		$this->load->view('stock/stock_list', $data);
		$this->load->view('stock_footer');
	}
			
	public function stock_view(){
		$id = $this->uri->segment('3');
		$where = array('id'=>$id);
		$data['details'] = $this->Common_model->all_record_result('salesforce.vehicle_stock__c', $where);
		$this->load->view('header');
		$this->load->view('stock/stock_view', $data);
		$this->load->view('stock_footer');
	}
				
	
	public function stock_insert(){
		$data = $this->input->post();
		foreach($data as $key=>$value){
			if(is_null($value) || $value == '')
				unset($data[$key]);
		}
	    $return_value = $this->Common_model->insertVal('salesforce.vehicle_stock__c',$data);
		$res['id'] = $return_value;
		$res['msg'] = 'Vehicle Added';
		echo json_encode($res); 
	}
		
	public function stockeditFetch(){
		$id = $this->input->post('stock_id'); 
	    $where = array('id'=>$id);
		$data['stock_details'] = $this->Common_model->all_record_result('salesforce.vehicle_stock__c', $where);
		echo json_encode($data);
	}

	public function stock_edit_insert(){
		$stockno=$this->input->post('name'); 
		$sellingdealer=$this->input->post('selling_dealer__c');
		$vehicletype=$this->input->post('vehicle_type__c');
		$fueltype=$this->input->post('fuel_type__c');
		$model=$this->input->post('model_name__c');
        $vin=$this->input->post('vin__c'); 
		$regno=$this->input->post('rego_no__c');
		$year=$this->input->post('manufacture_year__c');
		$bodytype=$this->input->post('body_type__c');
		$bodycolor=$this->input->post('body_colour__c');
		$price=$this->input->post('egc_price__c');
		$series=$this->input->post('series_c__c');
		$stock_id=$this->input->post('editID');
		if($price=='')
		  $price = 0;
	     $data = array('name'=> $stockno, 'vehicle_type__c'=> $vehicletype,'fuel_type__c'=>$fueltype,
		'model_name__c'=> $model, 'vin__c'=> $vin, 'rego_no__c'=> $regno, 'manufacture_year__c'=> $year, 
		'body_type__c'=> $bodytype, 'body_colour__c'=> $bodycolor, 'egc_price__c'=> $price, 'series_c__c'=> $series);
		$where = array('id'=>$stock_id);
	    $return_value = $this->Common_model->record_update('salesforce.vehicle_stock__c',$where,$data);
		$res['id'] = $return_value;
		$res['msg'] = 'Vehicle Updated';
		echo json_encode($res);      
	}
}
