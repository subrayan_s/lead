<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

 public function __construct()
		{
			    parent ::__construct();
				$this->load->database();
				$this->load->library('session');
				$this->load->helper('url');
				$this->load->model('Common_model');
					if($this->session->userdata("dealer")=='')
				{
				 redirect(base_url());
				}
	    }
	public function index()
	{
	
	if($this->session->userdata("dashboard")=='header_dashboard')
	{
	$this->load->view('dashboard/header_dashboard');
	}
	else if($this->session->userdata("dashboard")=='header_dashboard_bmw')
	{
	$this->load->view('dashboard/header_dashboard_bmw');
	}
	
		
			$this->load->view('dashboard/dashboard');
				$this->load->view('footer');
	}
}
