<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead extends CI_Controller {

	  public function __construct(){
		parent ::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Common_model');
		if($this->session->userdata("dealer")=='')
			 redirect(base_url());
	   }
	
	    public function index(){
			$dealer = $this->session->userdata("dealer");
			$where = array('selling_dealer__c'=>$dealer);
			$data['lead_details'] = $this->Common_model->all_record_result('salesforce.lead',$where,'id');
			$this->load->view('header');
			$this->load->view('lead/lead_list', $data);
			$this->load->view('footer');
	    }
		
			
      public function lead_view(){
			$id	   = $this->uri->segment('3');
			$where = array('id'=>$id);
			$table = 'salesforce.lead';
			$res   = $this->Common_model->all_record_result($table, $where);
			$data['details'] = $res[0];
			$this->load->view('header');
			$this->load->view('lead/lead_view', $data);
			$this->load->view('footer');
	    }
					
				
	
	public function lead_insert(){
		$data = $this->input->post();
		$data['createddate'] = date('Y-m-d h:i:s');
		$data['createdbyid'] = 1123;
		foreach($data as $key=>$value){
			if(is_null($value) || $value == '' || $key == 'editID')
				unset($data[$key]);
		}
		$data['name'] = $data['firstname'] .' '.$data['lastname'];
		$data['selling_dealer__c'] = $this->session->userdata("dealer");
	    $return_value = $this->Common_model->insertVal('salesforce.lead',$data);
		echo json_encode($return_value);
	}
		
		
	public function leadeditFetch(){
		$id = $this->input->post('lead_id'); 
		$where = array('id'=>$id);
		$data['lead_details'] = $this->Common_model->all_record_result('salesforce.lead', $where);	
		echo json_encode($data['lead_details']);
	}		

	public function lead_edit_insert(){
		$data      			  = $this->input->post();
		$lead_id              = $data['editID'];
		if(isset($data['eval_lead__c']))
			$data['eval_lead__c'] = ($data['eval_lead__c'] == 1)?'T':'F';

		if(isset($data['sent_to_dealer__c']))
			$data['sent_to_dealer__c'] = ($data['sent_to_dealer__c'] == 1)?'T':'F';
		if(isset($data['introduction_to_finance_manager__c']))
			$data['introduction_to_finance_manager__c'] = ($data['introduction_to_finance_manager__c'] == 1)?'T':'F';
		
		if(isset($data['appraisal_flag_field__c']))
			$data['appraisal_flag_field__c'] = ($data['appraisal_flag_field__c'] == 1)?'T':'F';
		if(isset($data['company']))
			$data['company'] = ($data['company'] == '')?$data['company']:'NA';
		
		$data['name'] = $data['firstname'] .' '.$data['lastname'];
		foreach($data as $key=>$value){
			if(is_null($value) || $value == '')
				unset($data[$key]);
		}		
		unset($data['editID']);	
		$where = array('id'=>$lead_id);
		$return_value = $this->Common_model->record_update('salesforce.lead',$where,$data);
		echo json_encode($return_value);
	}
	
	
	    public function initDataTable(){
  		    $draw = intval($this->input->get("draw"));
		    $start = intval($this->input->get("start"));
		    $length = intval($this->input->get("length"));
			$dealer = $this->session->userdata("dealer");
			$where  = array('selling_dealer__c'=>$dealer);
			$data2   = $this->Common_model->all_record_result('salesforce.lead',$where,'id');
			$i = 1;
			foreach($data2 as $key => $value) {
				$name_link = '<a href='.WEB_DIR.'lead/lead_view/'.$value->id.'>'.$value->name.'</a>';
				$edit_link = '<a data-toggle="modal" title="View Lead" data-id='.$value->id.' class="open-leadEdit" data-target="#createLeadModal"><span style="font-size:15px;" class="glyphicon glyphicon-edit"></span></a>';
				   $data[] = array(
						$i++,
						$name_link,
						$value -> company,
						$value -> email,
						$value -> mobilephone,
						$value -> lead_type__c,
						$value -> request_type__c,
						$value -> leadsource,
						$value -> status,
						$value -> vehicle_type__c,
						$value -> createddate,
						$edit_link,
				   );
			}
			$res = array(
				 "draw" => $draw,
                 "recordsTotal" => count($data2),
                 "recordsFiltered" => count($data2),
                 "data" => $data
            );
			echo json_encode($res);
	  }		
	  
	  public function getSearchData(){
			$column  = $this->input->post("column");
			$table   = 'salesforce.lead';
			$keyword = $this->input->post("keyword");
			$data = $this->Common_model->getSearchData($column,$table,$keyword);		
			echo json_encode($data);
	  }	  

	  public function oneFieldData(){
			$column  = $this->input->post("column");
			$table   = 'salesforce.lead';
			$data = $this->Common_model->oneFieldData($column,$table);echo json_encode($data);
	  }	  	  
	
}
