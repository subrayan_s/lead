﻿$(document).ready(function() {
	let dealer = [];
	$(".lead_update").on('click', function(event) {
        $('#createLeadModal :input:not(:button)').each(function(index, value) {
            if ($(this).parsley().validate() !== true) {
                decide_submit = false;
            }
        });		
	});	
    $("#insertAndEditForm").on('submit', function(event) {
		event.preventDefault();
        let decide_submit = true;
        let submit_type = $("#addOrEdit").val();
        $('#createLeadModal :input:not(:button)').each(function(index, value) {
            if ($(this).parsley().validate() !== true) {
                decide_submit = false;
            }
        });
        if (decide_submit) {
			$("#wait").show();
			let formAll  = $('#insertAndEditForm')[0];
			let jsonData = new FormData(formAll);
			let url;
			if(submit_type == 'add')
				url = 'lead/lead_insert';
			if(submit_type == 'edit')
				url = 'lead/lead_edit_insert';		
			//console.log($('#insertAndEditForm').serialize);
			$.ajax({
				url:url,
				method:'post',
				dataType: 'json',
				cache : false,
				data:jsonData,
				processData: false,
				contentType: false,
				success:function(data){
					console.log(data);
					$("#wait").hide();
					$(".clearData").trigger('click');
					$('#displayResult').html('<div class="alert alert-success"> <strong>Success! </strong> Lead Added <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');
					loadDataTable('lead/initDataTable');
				},
				error:function(data){
					$('#displayResult').html('<div class="alert alert-danger"> <strong>Failed! </strong> Operation Failed. <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');		
					$("#wait").hide();
					$(".clearData").trigger('click');
				}	
			});	
        }
    });
	
    $(".clearData").on('click', function(event) {
        $('#createLeadModal :input:not(:button)').each(function(index, value) {
            $(this).val('');
        });
    });
	/**pick list maker*/
	autocomplete(document.getElementById("country"), 'countries');
	autocomplete(document.getElementById("selling_dealer__c"), 'selling_dealer__c');
	
	
	$(document).on("click", ".fillColumnID", function (){
		let fld = $("#getSearchField").val();
		$("#"+fld).val($(this).attr('data'));
		$('#SearchModal').modal('hide');
		$('#myModal').modal('show');
		$("#selling_dealer__c_error").hide();		
	});	

	/* Search Data Ajax*/
	$(document).on("click", ".searchSD", function (){
		let value  = $("#sdname").val();
		let column = $("#getSearchField").val();
		if(value != ''){
			$("#wait").show();
			$.ajax({
				url:'lead/getSearchData',
				method:'post',
				data: {column:column,keyword:value},	
				success:function(data){
					data = JSON.parse(data);
					let app_data = '';
					for(let inc = 0; inc < data.length; inc++){
						app_data+='<tr><td align="center"><a class="fillColumnID" data="'+data[inc].selling_dealer__c+'" href="#">'+data[inc].selling_dealer__c+'</a></td></tr>';	
					}
					$(".Search_results_div").html('Search Results');
					$("#modalSearchRes").html(app_data);	
					$("#wait").hide();
				}	
			});	
		}	
	});	
});

function showSearchModal(type){
	$("#modalSearchRes").html('');
	$("#sdname").val('');
	$(".Search_results_div").html('');	
	$('#SearchModal').modal('show');
	$("#getSearchField").val(type);
}	

/** Lead Load.js */
var model=new Array();
function getmodel() {
	var make = $('#leadsource').val();
	simpleAppendLead(make);
}
function simpleAppendLead(make){
	if(make==='Phone'){
		$('#lead_type__c').html('');
		$('#lead_type__c').append("<option value='Online'>Online</option><option value='Radio'>Radio</option><option value='Edm'>Edm</option>");
	}
	if(make===''){
		$('#lead_type__c').html('');
		$('#lead_type__c').append("<option value=''>--SELECT--</option>");
	}
	if(make==='Walk-In'){
		$('#lead_type__c').html('');
		$('#lead_type__c').append("<option value='Online'>Online</option><option value='Radio'>Radio</option><option value='Edm'>Edm</option>");
	}
	if(make==='Internet'){
		$('#lead_type__c').html('');
		$('#lead_type__c').append("<option value='Gumtree'>Gumtree</option><option value='Carsales'>Carsales</option><option value='Facebook'>Facebook</option>");
	}
	
}	
$(document).on("change", "#leadsource", function (){
		simpleAppendLead($(this).val());
});	
/** Updated leadload ends here */
$(document).on("blur", "#selling_dealer__c", function (){
	setTimeout(function(){
		let val = $("#selling_dealer__c").val();
		console.log(val);
		$("#selling_dealer__c_error").show();
		if($.inArray(val, dealer) != -1){
			console.log('if');
			$("#selling_dealer__c_error").hide();
		} else {
			$("#selling_dealer__c_error").html('<p style="color:red"> Please pick an option from list</p>');
			$("#selling_dealer__c_error").show();
		}	
	 }, 400);
});	
