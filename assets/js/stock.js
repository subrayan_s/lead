﻿$(document).ready(function() {
	$("#newStockItem").on('click', function(event) {
		$("#stockTitle").html('New Stock');
		$("#addOrEdit").val('add');
	});
   $(document).on("click", ".openStockEdit", function (){
		$("#stockTitle").html('Edit Stock');
		$("#addOrEdit").val('edit');
		$("#editIDadd").val($(this).attr('data-id'));
		$("#wait").show();
		let url = "stock/stockeditFetch";
		$.ajax({
			url: url,
			type: 'POST',
			data: { stock_id : $(this).attr('data-id') },
			dataType: 'json',
			cache: false,
			success: function (data){
				console.log(data);
				let stock_details = data.stock_details[0];
				$.map(stock_details, function(value, index) {
					$("#"+index).val(value);
				});	
				$("#createddate").html(data.stock_details[0].createddate)
				$("#wait").hide();
			},
			error:function(){
				$("#wait").hide();
				console.log(data);
			}	
		});		
		
	});	
	$(".stock_update").on('click', function(event) {
        $('#createLeadModal :input:not(:button)').each(function(index, value) {
            if ($(this).parsley().validate() !== true) {
                decide_submit = false;
            }
        });		
	});	
    $("#stockForm").on('submit', function(event) {
		event.preventDefault();
        let decide_submit = true;
        let submit_type = $("#addOrEdit").val();
        console.log(submit_type);
        $('#createLeadModal :input:not(:button)').each(function(index, value) {
            if ($(this).parsley().validate() !== true) {
                decide_submit = false;
            }
        });
		console.log('here'+submit_type);
        if (decide_submit) {
			$("#wait").show();
			let formAll  = $('#stockForm')[0];
			let jsonData = new FormData(formAll);
			let url = '';
			if(submit_type == 'add'){
				url = 'stock/stock_insert';
			}	
			if(submit_type == 'edit'){
				let id = $("#editIDadd").val();
				url = 'stock/stock_edit_insert';
			}
			$.ajax({
				url:url,
				method:'post',
				dataType: 'json',
				cache : false,
				data:jsonData,
				processData: false,
				contentType: false,
				success:function(data){
					console.log(data);
					$("#wait").hide();
					$(".clearData").trigger('click');
					$('#displayResult').html('<div class="alert alert-success"> <strong>Success! </strong>'+data.msg+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');					
				},
				error:function(data){
					$('#displayResult').html('<div class="alert alert-danger"> <strong>Failed! </strong> Operation Failed. <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>');		
					$("#wait").hide();
					$(".clearData").trigger('click');
				}	
			});	
        }
    });
	
    $(".clearData").on('click', function(event) {
        $('#createLeadModal :input:not(:button)').each(function(index, value) {
            $(this).val('');
        });
    });
});